package com.app.myswipe.Services;

/**
 * Created by yuvaraj on 02/12/17.
 */

public class EmojiMessageEvent {
    String type;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public EmojiMessageEvent(String type) {

        this.type = type;
    }

    public EmojiMessageEvent() {
    }


}
