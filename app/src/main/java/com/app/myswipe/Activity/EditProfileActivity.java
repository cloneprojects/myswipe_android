package com.app.myswipe.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.Helper.UrlHelper;
import com.app.myswipe.Helper.Utils;
import com.app.myswipe.Instagram.InstagramApp;
import com.app.myswipe.R;
import com.app.myswipe.Volley.ApiCall;
import com.app.myswipe.Volley.VolleyCallback;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView firstImage, secondImage, thirdImage, fourthImage, fifthImage, sixthImage;
    EditText aboutYou, jobTitle, companyName, collegeName;
    RadioGroup genderGroup;
    RadioButton maleRadioButton, femaleRadioButton;
    Switch ageSwitch, distanceSwitch;
    LinearLayout disconnectedLayout;
    ImageView backIcon;
    LinearLayout ageLayout, distanceLayout;
    private FrameLayout firstAction, secondAction, thirdAction, fourthAction, fifthAction, sixthAction;
    private ImageView firstImageEmpty, secondImageEmpty, thirdImageEmpty, fourthImageEmpty, fifthImageEmpty, sixthImageEmpty;
    private ImageView firstImageAction, secondImageAction, thirdImageAction, fourthImageAction, fifthImageAction, sixthImageAction;
    int imageSelected;
    String firstImageUrl = "", secondImageUrl = "", thirdImageUrl = "", fourthImageUrl = "", fifthImageUrl = "", sixthImageUrl = "";
    String firstImageId = "", secondImageId = "", thirdImageId = "", fourthImageId = "", fifthImageId = "", sixthImageId = "";
    private String TAG = EditProfileActivity.class.getSimpleName();
    private File displayPicture;
    private Uri uri;
    private String displayPicUrl;
    TextView disconnectText, instagramUserName;
    RelativeLayout connectedLayout;
    JSONArray imageArray = new JSONArray();
    private AppSettings appSettings = new AppSettings(EditProfileActivity.this);


    private static final String CLIENT_ID = "b3a7ab2a98db4c70a02b3fb5f94ce5eb";
    private static final String CLIENT_SECRET = "0a2f151660d94774a78ac13daea158ad";
    public static final String CALLBACK_URL = "http://myswipe.co.uk/";

    private String filePath;
    private String path;
    private String finalImageUrl;
    private InstagramApp mApp;
    public static int WHAT_FINALIZE = 0;

    private AdView mAdView;
    private AdRequest adRequest;

    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == WHAT_FINALIZE) {
                Log.d(TAG, "handleMessage: " + mApp.getId() + "/ " + mApp.getTOken());

                userInfoHashmap = mApp.getUserInfo();

                try {
                    updateTokenandId();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (msg.what == WHAT_FINALIZE) {
                Toast.makeText(EditProfileActivity.this, "Check your network.",
                        Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });

    private void updateTokenandId() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("senderID", mApp.getId());
        jsonObject.put("instaToken", mApp.getTOken());
        jsonObject.put("instaName", mApp.getUserName());
        ApiCall.PostMethod(EditProfileActivity.this, UrlHelper.ACTIVATE_INSTAGRAM, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                setInstaValues();


            }
        });
    }

    private void setInstaValues() {
        appSettings.setIsInstagramConnected("true");
        appSettings.setInstagramName(mApp.getUserName());
        appSettings.setInstagramToken(mApp.getTOken());
        appSettings.setInstagramId(mApp.getId());
        setConnectedLayout();
    }


    private void setConnectedLayout() {
        instagramUserName.setText(appSettings.getInstagramName());

        connectedLayout.setVisibility(View.VISIBLE);
        disconnectedLayout.setVisibility(View.GONE);
    }

    public void setDisconnectedLayout() {
        connectedLayout.setVisibility(View.GONE);
        disconnectedLayout.setVisibility(View.VISIBLE);
        resetInstaValues();


    }


//    private InstagramHelper instagramHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initViews();
        initListners();
        loadData();
        initInstagram();

    }

    private void initInstagram() {

        String scope = "basic";

//        instagramHelper = new InstagramHelper.Builder()
//                .withClientId(CLIENT_ID)
//                .withRedirectUrl(REDIRECT_URI)
//                .withScope(scope)
//                .build();
    }


    @Override
    public void onBackPressed() {
        try {
            updateDataToserver();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void loadData() {
        imageArray = appSettings.getImagesArray();
        for (int i = 0; i < imageArray.length(); i++) {
            JSONObject jsonObject = imageArray.optJSONObject(i);
            String imageUrl = jsonObject.optString("imageURL");
            String id = jsonObject.optString("id");

            if (i == 0) {
                firstImageUrl = imageUrl;
                firstImageId = id;
            } else if (i == 1) {
                secondImageUrl = imageUrl;
                secondImageId = id;


            } else if (i == 2) {
                thirdImageUrl = imageUrl;
                thirdImageId = id;


            } else if (i == 3) {
                fourthImageUrl = imageUrl;
                fourthImageId = id;


            } else if (i == 4) {
                fifthImageUrl = imageUrl;
                fifthImageId = id;


            } else if (i == 5) {
                sixthImageUrl = imageUrl;
                sixthImageId = id;

            }

        }

        aboutYou.setText(appSettings.getAboutUs());
        jobTitle.setText(appSettings.getJobTitle());
        companyName.setText(appSettings.getCompany());
        collegeName.setText(appSettings.getCollege());
        if (appSettings.getGender().equalsIgnoreCase("Male")) {
            maleRadioButton.setChecked(true);
            femaleRadioButton.setChecked(false);
        } else {
            maleRadioButton.setChecked(false);

            femaleRadioButton.setChecked(true);
        }
        if (appSettings.getShowAge().equalsIgnoreCase("1")) {
            ageSwitch.setChecked(true);
        } else {
            ageSwitch.setChecked(false);
        }

        if (appSettings.getShowDistance().equalsIgnoreCase("1")) {
            distanceSwitch.setChecked(true);
        } else {
            distanceSwitch.setChecked(false);
        }
        loadImageFromUrls();
    }

    private void initListners() {
        firstAction.setOnClickListener(this);
        secondAction.setOnClickListener(this);
        thirdAction.setOnClickListener(this);
        fourthAction.setOnClickListener(this);
        fifthAction.setOnClickListener(this);
        sixthAction.setOnClickListener(this);
        backIcon.setOnClickListener(this);
        disconnectText.setOnClickListener(this);

    }

    private void initViews() {

        mAdView = (AdView) findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();

        if (appSettings.getUserType().equalsIgnoreCase("basic")) {
            mAdView.setVisibility(View.VISIBLE);
            mAdView.loadAd(adRequest);
        } else {
            mAdView.setVisibility(View.GONE);
        }
        backIcon = (ImageView) findViewById(R.id.backIcon);

        instagramUserName = (TextView) findViewById(R.id.instagramUserName);
        disconnectText = (TextView) findViewById(R.id.disconnectText);


        firstImage = (ImageView) findViewById(R.id.firstImage);
        secondImage = (ImageView) findViewById(R.id.secondImage);
        thirdImage = (ImageView) findViewById(R.id.thirdImage);
        fourthImage = (ImageView) findViewById(R.id.fourthImage);
        fifthImage = (ImageView) findViewById(R.id.fifthImage);
        sixthImage = (ImageView) findViewById(R.id.sixthImage);


        firstAction = (FrameLayout) findViewById(R.id.firstAction);
        secondAction = (FrameLayout) findViewById(R.id.secondAction);
        thirdAction = (FrameLayout) findViewById(R.id.thirdAction);
        fourthAction = (FrameLayout) findViewById(R.id.fourthAction);
        fifthAction = (FrameLayout) findViewById(R.id.fifthAction);
        sixthAction = (FrameLayout) findViewById(R.id.sixthAction);

        connectedLayout = (RelativeLayout) findViewById(R.id.connectedLayout);

        firstImageEmpty = (ImageView) findViewById(R.id.firstImageEmpty);
        secondImageEmpty = (ImageView) findViewById(R.id.secondImageEmpty);
        thirdImageEmpty = (ImageView) findViewById(R.id.thirdImageEmpty);
        fourthImageEmpty = (ImageView) findViewById(R.id.fourthImageEmpty);
        fifthImageEmpty = (ImageView) findViewById(R.id.fifthImageEmpty);
        sixthImageEmpty = (ImageView) findViewById(R.id.sixthImageEmpty);


        firstImageAction = (ImageView) findViewById(R.id.firstImageAction);
        secondImageAction = (ImageView) findViewById(R.id.secondActionImage);
        thirdImageAction = (ImageView) findViewById(R.id.thirdActionImage);
        fourthImageAction = (ImageView) findViewById(R.id.fourthActionImage);
        fifthImageAction = (ImageView) findViewById(R.id.fifthActionImage);
        sixthImageAction = (ImageView) findViewById(R.id.sixthActionImage);


        aboutYou = (EditText) findViewById(R.id.aboutYou);
        jobTitle = (EditText) findViewById(R.id.jobTitle);
        companyName = (EditText) findViewById(R.id.companyName);
        collegeName = (EditText) findViewById(R.id.collegeName);
        genderGroup = (RadioGroup) findViewById(R.id.genderGroup);
        maleRadioButton = (RadioButton) findViewById(R.id.maleRadioButton);
        femaleRadioButton = (RadioButton) findViewById(R.id.femaleRadioButton);
        ageSwitch = (Switch) findViewById(R.id.ageSwitch);
        distanceSwitch = (Switch) findViewById(R.id.distanceSwitch);
        ageLayout = (LinearLayout) findViewById(R.id.ageLayout);
        distanceLayout = (LinearLayout) findViewById(R.id.distanceLayout);
        disconnectedLayout = (LinearLayout) findViewById(R.id.disconnectedLayout);

        ageSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (ageSwitch.isChecked()) {
                    appSettings.setShowAge("1");
                } else {
                    appSettings.setShowAge("0");
                }
            }
        });

        distanceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (distanceSwitch.isChecked()) {
                    appSettings.setShowDistance("1");
                } else {
                    appSettings.setShowDistance("0");
                }
            }
        });

        disconnectedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                mApp = new InstagramApp(EditProfileActivity.this, CLIENT_ID,
                        CLIENT_SECRET, CALLBACK_URL);

                mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

                    @Override
                    public void onSuccess() {

                        mApp.fetchUserName(handler);
                        Log.d(TAG, "onSuccess: " + mApp.getName());

                    }

                    @Override
                    public void onFail(String error) {
                        Toast.makeText(EditProfileActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                });

                mApp.authorize();
//                instagramHelper.loginFromActivity(EditProfileActivity.this);
            }
        });


        if (appSettings.getIsInstagramConnected().equalsIgnoreCase("true")) {
            setConnectedLayout();
        } else {
            setDisconnectedLayout();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public int checkEmptyStrings() {

        int i = 6;

        if (firstImageUrl.length() == 0) {
            i = i - 1;
        }
        if (secondImageUrl.length() == 0) {
            i = i - 1;
        }
        if (thirdImageUrl.length() == 0) {
            i = i - 1;
        }
        if (fourthImageUrl.length() == 0) {
            i = i - 1;
        }
        if (fifthImageUrl.length() == 0) {
            i = i - 1;
        }
        if (sixthImageUrl.length() == 0) {
            i = i - 1;
        }
        return i;

    }

    @Override
    public void onClick(View view) {
        if (view == firstAction) {

            if (firstImageUrl.length() != 0) {

                try {
                    deleteImage(0, firstImageId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                imageSelected = 1;
                showPictureDialog();
            }

        } else if (view == secondAction) {
            if (secondImageUrl.length() != 0) {
                try {
                    deleteImage(1, secondImageId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                imageSelected = 2;
                showPictureDialog();
            }

        } else if (view == thirdAction) {
            if (thirdImageUrl.length() != 0) {

                try {
                    deleteImage(2, thirdImageId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                imageSelected = 3;
                showPictureDialog();
            }
        } else if (view == fourthAction) {
            if (fourthImageUrl.length() != 0) {


                try {
                    deleteImage(3, fourthImageId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                imageSelected = 4;
                showPictureDialog();
            }
        } else if (view == fifthAction) {
            if (fifthImageUrl.length() != 0) {
                try {
                    deleteImage(4, fifthImageId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                imageSelected = 5;
                showPictureDialog();
            }
        } else if (view == sixthAction) {
            if (sixthImageUrl.length() != 0) {

                try {
                    deleteImage(5, sixthImageId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                imageSelected = 6;
                showPictureDialog();
            }
        } else if (view == backIcon) {
            try {
                updateDataToserver();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (view == disconnectText) {
            try {
                disconnectInstagram();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void disconnectInstagram() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethod(EditProfileActivity.this, UrlHelper.DEACTIVATE_INSTAGRAM, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                setDisconnectedLayout();


            }
        });
    }

    private void resetInstaValues() {
        appSettings.setInstagramName("");
        appSettings.setTotalCount("0");
        appSettings.setIsInstagramConnected("false");
        JSONArray jsonArray = new JSONArray();
        appSettings.setInstaGramPhotos(jsonArray);
    }


    private void insertImage(final String finalImageUrl) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("imageURL", finalImageUrl);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodHeaders(EditProfileActivity.this, UrlHelper.INSERT_IMAGE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                if (firstImageUrl.trim().length() == 0) {
                    firstImageUrl = finalImageUrl;
                } else if (secondImageUrl.trim().length() == 0) {
                    secondImageUrl = finalImageUrl;


                } else if (thirdImageUrl.trim().length() == 0) {
                    thirdImageUrl = finalImageUrl;


                } else if (fourthImageUrl.trim().length() == 0) {
                    fourthImageUrl = finalImageUrl;


                } else if (fifthImageUrl.trim().length() == 0) {
                    fifthImageUrl = finalImageUrl;


                } else if (sixthImageUrl.trim().length() == 0) {
                    sixthImageUrl = finalImageUrl;
                }

                loadImageFromUrls();


            }
        });


    }


    private void deleteImage(final int i, String ImageId) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", ImageId);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodHeaders(EditProfileActivity.this, UrlHelper.DELETE_IMAGE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                if (i == 0) {
                    prevOneIcons();
                } else if (i == 1) {
                    prevTwoIcons();
                } else if (i == 2) {
                    prevThreeIcons();
                } else if (i == 3) {
                    prevFourIcons();
                } else if (i == 4) {
                    prevFiveIcons();
                } else {
                    prevSixIcons();
                }

            }
        });


    }

    private void prevOneIcons() {
        firstImageUrl = secondImageUrl;
        secondImageUrl = thirdImageUrl;
        thirdImageUrl = fourthImageUrl;
        fourthImageUrl = fifthImageUrl;
        fifthImageUrl = sixthImageUrl;
        sixthImageUrl = "";
        sixthImage.setImageResource(android.R.color.transparent);

        loadImageFromUrls();

    }


    private void prevTwoIcons() {
        secondImageUrl = thirdImageUrl;
        thirdImageUrl = fourthImageUrl;
        fourthImageUrl = fifthImageUrl;
        fifthImageUrl = sixthImageUrl;
        sixthImageUrl = "";
        sixthImage.setImageResource(android.R.color.transparent);

        loadImageFromUrls();

    }

    private void prevThreeIcons() {
        thirdImageUrl = fourthImageUrl;
        fourthImageUrl = fifthImageUrl;
        fifthImageUrl = sixthImageUrl;
        sixthImageUrl = "";
        sixthImage.setImageResource(android.R.color.transparent);

        loadImageFromUrls();

    }

    private void prevFourIcons() {
        fourthImageUrl = fifthImageUrl;
        fifthImageUrl = sixthImageUrl;
        sixthImageUrl = "";
        sixthImage.setImageResource(android.R.color.transparent);

        loadImageFromUrls();

    }

    private void prevFiveIcons() {
        fifthImageUrl = sixthImageUrl;
        sixthImageUrl = "";
        sixthImage.setImageResource(android.R.color.transparent);
        loadImageFromUrls();

    }

    private void prevSixIcons() {
        sixthImageUrl = "";
        sixthImage.setImageResource(android.R.color.transparent);
        loadImageFromUrls();

    }


    private void updateDataToserver() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject = getJsonValues();

        ApiCall.PostMethod(EditProfileActivity.this, UrlHelper.EDIT_PROFILE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                try {
                    getProfileData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.dismiss(EditProfileActivity.this);
    }

    private void getProfileData() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodHeaders(EditProfileActivity.this, UrlHelper.VIEW_PROFILE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                JSONArray jsonArray = response.optJSONArray("results");
                JSONObject object = jsonArray.optJSONObject(0);

                appSettings.setUserId(object.optString("id"));
                appSettings.setUserImageUrl(object.optJSONArray("images").optJSONObject(0).optString("imageURL"));
                appSettings.setAboutUs(object.optString("aboutUser"));
                appSettings.setJobTitle(object.optString("jobTitle"));
                appSettings.setCompany(object.optString("company"));
                appSettings.setCollege(object.optString("school"));
                appSettings.setImagesArray(object.optJSONArray("images"));
                appSettings.setGender(object.optString("gender"));
                appSettings.setShowAge(object.optString("dontShowAge"));
                appSettings.setShowDistance(object.optString("makeDistanceInvisible"));
                if (object.optString("InstaStatus").equalsIgnoreCase("connect")) {
                    appSettings.setIsInstagramConnected("true");
                    appSettings.setInstagramName(object.optString("InstaName"));
                    appSettings.setInstagramToken(object.optString("InstaToken"));
                    appSettings.setInstagramId(object.optString("InstaSenderID"));
                } else {
                    appSettings.setInstagramName("");
                    appSettings.setTotalCount("0");
                    appSettings.setIsInstagramConnected("false");
                    JSONArray jsonArrays = new JSONArray();
                    appSettings.setInstaGramPhotos(jsonArrays);
                }
//                ProfileFragment.setData(EditProfileActivity.this);
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK);
                finish();


            }
        });
    }


    private JSONObject getJsonValues() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("aboutUser", aboutYou.getText().toString());
        jsonObject.put("jobTitle", jobTitle.getText().toString());
        jsonObject.put("company", companyName.getText().toString());
        jsonObject.put("school", collegeName.getText().toString());
        if (maleRadioButton.isChecked()) {
            jsonObject.put("gender", "Male");


        } else {
            jsonObject.put("gender", "Female");

        }
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("dontShowAge", appSettings.getShowAge());
        jsonObject.put("makeDistanceInvisible", appSettings.getShowDistance());
//        JSONArray jsonArray = new JSONArray();
//        for (int i = 0; i < 6; i++) {
//            JSONObject object = new JSONObject();
//            object.put("imageNumber", i + 1);
//
//            if (i == 0) {
//
//                object.put("imageURL", firstImageUrl);
//            } else if (i == 1) {
//                object.put("imageURL", secondImageUrl);
//
//
//            } else if (i == 2) {
//                object.put("imageURL", thirdImageUrl);
//
//
//            } else if (i == 3) {
//                object.put("imageURL", fourthImageUrl);
//
//
//            } else if (i == 4) {
//                object.put("imageURL", fifthImageUrl);
//
//
//            } else if (i == 5) {
//                object.put("imageURL", sixthImageUrl);
//
//            }
//            jsonArray.put(object);
//
//
//        }
//        jsonObject.put("images", jsonArray.toString());

        return jsonObject;
    }

    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
                        } else {
                            takePhotoFromCamera();
                        }

                        break;

                }
            }
        });
        if (firstImageUrl.length() == 0) {

            dialog.setCancelable(false);

        } else {
            dialog.setCancelable(true);
        }
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 100);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100:
                //gallery
                if (resultCode == RESULT_OK) {


                    if (data != null) {
                        uri = data.getData();
                        if (uri != null) {
                            beginCrop(uri);
                        } else {
                            Utils.toast(EditProfileActivity.this, getString(R.string.unable_to_select));
                        }
                    }
                } else {
                    loadImageFromUrls();
                }
                break;


            case 104:
                //camera
                if (resultCode == RESULT_CANCELED) {
                    loadImageFromUrls();

                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        if (uri != null) {
                            beginCrop(uri);
                        } else {
                            Utils.toast(EditProfileActivity.this, getString(R.string.unable_to_select));
                        }

                    } else {
                        Log.d(TAG, "onActivityResult: " + appSettings.getImageValue());
                        if (appSettings.getImageValue().trim().length() != 0) {
                            File file = new File(appSettings.getImageValue());
                            beginCrop(Uri.fromFile(file));
                        } else {
                            Utils.toast(EditProfileActivity.this, getString(R.string.unable_to_select));
                        }

                    }
                }
                break;

            case Crop.REQUEST_CROP:

                if (data != null)
                    handleCrop(resultCode, data);
                break;


        }
    }


    private void beginCrop(Uri source) {
        //String register_id = code + number;
        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), Calendar.getInstance().getTimeInMillis() + ".jpg"));
        Crop.of(source, outputUri).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {

        if (resultCode == RESULT_OK) {
            filePath = Utils.getRealPathFromUriNew(EditProfileActivity.this, Crop.getOutput(result));

            Log.e("image_path", "" + filePath);

            displayPicture = new File(filePath);

            uploadImage();

        } else if (resultCode == Crop.RESULT_ERROR) {
            Utils.toast(EditProfileActivity.this, getResources().getString(R.string.unable_to));
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    public void loadImageFromUrls() {

        if (firstImageUrl.trim().length() == 0) {
            firstImage.setImageResource(android.R.color.transparent);

            imageSelected = 1;
            showPictureDialog();

        } else {
            Glide.with(EditProfileActivity.this).load(firstImageUrl).into(firstImage);
        }

        if (secondImageUrl.trim().length() == 0) {
            secondImage.setImageResource(android.R.color.transparent);


        } else {
            Glide.with(EditProfileActivity.this).load(secondImageUrl).into(secondImage);
        }

        if (thirdImageUrl.trim().length() == 0) {
            thirdImage.setImageResource(android.R.color.transparent);


        } else {
            Glide.with(EditProfileActivity.this).load(thirdImageUrl).into(thirdImage);
        }

        if (fourthImageUrl.trim().length() == 0) {
            fourthImage.setImageResource(android.R.color.transparent);


        } else {
            Glide.with(EditProfileActivity.this).load(fourthImageUrl).into(fourthImage);
        }

        if (fifthImageUrl.trim().length() == 0) {
            fifthImage.setImageResource(android.R.color.transparent);


        } else {
            Glide.with(EditProfileActivity.this).load(fifthImageUrl).into(fifthImage);
        }

        if (sixthImageUrl.trim().length() == 0) {
            sixthImage.setImageResource(android.R.color.transparent);


        } else {
            Glide.with(EditProfileActivity.this).load(sixthImageUrl).into(sixthImage);
        }


        setIcons();


    }

    private void setIcons() {
        if (firstImageUrl.length() == 0) {
            firstImageAction.setVisibility(View.GONE);
            firstImageEmpty.setVisibility(View.VISIBLE);
        } else {

            firstImageAction.setVisibility(View.VISIBLE);
            firstImageEmpty.setVisibility(View.GONE);
        }


        if (secondImageUrl.length() == 0) {
            secondImageAction.setVisibility(View.GONE);
            secondImageEmpty.setVisibility(View.VISIBLE);

        } else {
            secondImageAction.setVisibility(View.VISIBLE);
            secondImageEmpty.setVisibility(View.GONE);
        }

        if (thirdImageUrl.length() == 0) {
            thirdImageAction.setVisibility(View.GONE);
            thirdImageEmpty.setVisibility(View.VISIBLE);

        } else {
            thirdImageAction.setVisibility(View.VISIBLE);
            thirdImageEmpty.setVisibility(View.GONE);

        }

        if (fourthImageUrl.length() == 0) {
            fourthImageAction.setVisibility(View.GONE);
            fourthImageEmpty.setVisibility(View.VISIBLE);

        } else {
            fourthImageAction.setVisibility(View.VISIBLE);
            fourthImageEmpty.setVisibility(View.GONE);

        }

        if (fifthImageUrl.length() == 0) {
            fifthImageAction.setVisibility(View.GONE);
            fifthImageEmpty.setVisibility(View.VISIBLE);


        } else {
            fifthImageAction.setVisibility(View.VISIBLE);
            fifthImageEmpty.setVisibility(View.GONE);

        }
        if (sixthImageUrl.length() == 0) {
            sixthImageAction.setVisibility(View.GONE);
            sixthImageEmpty.setVisibility(View.VISIBLE);

        } else {
            sixthImageAction.setVisibility(View.VISIBLE);
            sixthImageEmpty.setVisibility(View.GONE);

        }


    }

    private void handleCameraImage() {
        displayPicUrl = appSettings.getImagePath();
        displayPicture = new File(displayPicUrl);

        uploadImage();


    }

    private void handleimage(Uri uri) {


        displayPicUrl = Utils.getRealPathFromURI(EditProfileActivity.this, uri);
        displayPicture = new File(displayPicUrl);


        uploadImage();


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;


            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.storage_permission_error));

                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    private void takePhotoFromCamera() {


        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(), getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder, getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }


        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("PROFILE_IMG_" + randomImageNo);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

//        uri = Uri.fromFile(file);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(EditProfileActivity.this, getPackageName() + ".provider", file);
            Log.d(TAG, "onClick: " + uri.getPath());
            appSettings.setImagePath(uri.getPath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, 104);
        } else {
            uri = Uri.fromFile(file);

            path = file.getPath();
            appSettings.setImageValue(path);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, 104);
        }


    }


    public class uploadToserver extends AsyncTask<Void, Void, Void> {


        protected void onPreExecute() {
            Utils.show(EditProfileActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {


            ApiCall.uploadImage(displayPicture, EditProfileActivity.this, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.d(TAG, "onSuccess: " + response);
                    finalImageUrl = response.optString("URL");


//
//                    if (imageSelected == 1) {
//                        firstImageUrl = finalImageUrl;
//                    } else if (imageSelected == 2) {
//                        secondImageUrl = finalImageUrl;
//
//
//                    } else if (imageSelected == 3) {
//                        thirdImageUrl = finalImageUrl;
//
//
//                    } else if (imageSelected == 4) {
//                        fourthImageUrl = finalImageUrl;
//
//
//                    } else if (imageSelected == 5) {
//                        fifthImageUrl = finalImageUrl;
//
//
//                    } else if (imageSelected == 6) {
//                        sixthImageUrl = finalImageUrl;
//                    }
//


                }
            });
            return null;
        }

        protected void onPostExecute(Void result) {


            Utils.dismiss(EditProfileActivity.this);

            try {
                insertImage(finalImageUrl);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    public void uploadImage() {

        new uploadToserver().execute();
    }

}
