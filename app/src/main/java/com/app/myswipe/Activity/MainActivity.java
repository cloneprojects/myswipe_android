package com.app.myswipe.Activity;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.WindowManager;

import com.app.myswipe.Fragment.ChatFragment;
import com.app.myswipe.Fragment.HomeFragment;
import com.app.myswipe.Fragment.ProfileFragment;
import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.Helper.Utils;
import com.app.myswipe.R;
import com.app.myswipe.Services.ServiceClass;


import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    public static ViewPager viewPager;
    public static TabLayout tabLayout;
    ViewPagerAdapter adapter;
    CardView bottomCard;
    AppSettings appSettings = new AppSettings(MainActivity.this);
    HomeFragment homeFragment = new HomeFragment();
    ChatFragment chatFragment = new ChatFragment();
    ProfileFragment profileFragment = new ProfileFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initTabs();
        initService();

//        Intent intent=new Intent(MainActivity.this,ChatActivity.class);
//        startActivity(intent);

    }


    private void initService() {
        if (!isMyServiceRunning(ServiceClass.class)) {
            MainActivity.this.startService(new Intent(MainActivity.this, ServiceClass.class));
        } else {
            ServiceClass.Emitters emitters = new ServiceClass.Emitters(MainActivity.this);
            emitters.emitonline();
        }
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initTabs() {

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        bottomCard = (CardView) findViewById(R.id.bottomCard);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.bottomBar);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            bottomCard.setCardElevation(10);
        }


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
               getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                Utils.hideKeyboard(MainActivity.this);

                if (position == 0) {
                    setupTabIcons();

                } else if (position == 1) {
                    try {
//                        chatFragment.getChatLists();
                        chatFragment.getNewMatchesDatas();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    setupTabIconsTwo();
                } else if (position == 2) {
                    setupTabIconsThree();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.tinder_icon);
        tabLayout.getTabAt(1).setIcon(R.drawable.bottom_chat_grey);
        tabLayout.getTabAt(2).setIcon(R.drawable.bottom_person_grey);

    }

    private void setupTabIconsTwo() {
        tabLayout.getTabAt(0).setIcon(R.drawable.tinder_grey);
        tabLayout.getTabAt(1).setIcon(R.drawable.bottom_chat_color);
        tabLayout.getTabAt(2).setIcon(R.drawable.bottom_person_grey);

    }

    private void setupTabIconsThree() {
        tabLayout.getTabAt(0).setIcon(R.drawable.tinder_grey);
        tabLayout.getTabAt(1).setIcon(R.drawable.bottom_chat_grey);
        tabLayout.getTabAt(2).setIcon(R.drawable.bottom_person_color);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.hideKeyboard(MainActivity.this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(homeFragment, "Home");
        adapter.addFrag(chatFragment, "Chat");
        adapter.addFrag(profileFragment, "Profile ");

        viewPager.setAdapter(adapter);

        try {
            if (getIntent().getStringExtra("tab").equalsIgnoreCase("1")) {
                viewPager.setCurrentItem(1);
                setupTabIconsTwo();
            }
        } catch (Exception e) {

        }
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mfragmentlist = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mfragmentlist.get(position);
        }

        @Override
        public int getCount() {
            return mfragmentlist.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mfragmentlist.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }


}
