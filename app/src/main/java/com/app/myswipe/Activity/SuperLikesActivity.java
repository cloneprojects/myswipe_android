package com.app.myswipe.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.app.myswipe.Adapters.SuperLikesAdapter;
import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.Helper.UrlHelper;
import com.app.myswipe.R;
import com.app.myswipe.Volley.ApiCall;
import com.app.myswipe.Volley.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SuperLikesActivity extends AppCompatActivity {
    private static final String TAG = SuperLikesActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private AppSettings appSettings;
    private SuperLikesAdapter superLikesAdapter;
    private JSONArray mJsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_likes);
        appSettings = new AppSettings(SuperLikesActivity.this);
        recyclerView = findViewById(R.id.superLikesRv);
        ImageView backIcon = findViewById(R.id.backIcon);
        loadList();

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });
    }

    private void loadList() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("accessToken", appSettings.getAccessToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethod(SuperLikesActivity.this, UrlHelper.SEE_LIKED_LIST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                mJsonArray = response.optJSONArray("LikedPerson");
                if (mJsonArray.length() == 0) {
                    recyclerView.setVisibility(View.GONE);

                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    superLikesAdapter = new SuperLikesAdapter(SuperLikesActivity.this, mJsonArray);
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(SuperLikesActivity.this, 2);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    recyclerView.setAdapter(superLikesAdapter);
                }
            }
        });
    }
}