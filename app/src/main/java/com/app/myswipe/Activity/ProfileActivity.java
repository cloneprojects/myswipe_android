package com.app.myswipe.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ecloud.pulltozoomview.PullToZoomScrollViewEx;
import com.app.myswipe.Fragment.InstagramPhotoFragment;
import com.app.myswipe.Fragment.ViewPagerFragment;
import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.Helper.UrlHelper;
import com.app.myswipe.R;
import com.app.myswipe.Volley.ApiCall;
import com.app.myswipe.Volley.VolleyCallback;
import com.viewpagerindicator.LinePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity {

    private ViewPager viewPager;
    String[] backgrounds = new String[0];
    LinearLayout optionsLayout;
    TextView userName, designation, about, age, location, instagramPhotoCount;
    JSONObject jsonObject = new JSONObject();
    RelativeLayout nopeButton, likeButton, superLikeButton;
    LinearLayout workingLayout, instagramLayout;
    private String TAG = ProfileActivity.class.getSimpleName();
    String instaId, instaToken;
    AppSettings appSettings = new AppSettings(ProfileActivity.this);
    private String[] instaGram;
    private ViewPager instaviewpager;
    boolean isConnected = false;
    boolean shouldShow = false;
    private LinearLayout aboutLayout, bottomLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        try {
            jsonObject = new JSONObject(getIntent().getStringExtra("profileDetails"));
            if (jsonObject.optString("InstaStatus").equalsIgnoreCase("connect")) {
                instaId = jsonObject.optString("InstaSenderID");
                instaToken = jsonObject.optString("InstaToken");
                isConnected = true;

            } else {
                isConnected = false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        loadViewForCode();
        try {
            initViews();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListners();


    }


    private void initListners() {
        optionsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSortPopup(view);
            }
        });

        nopeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishIntent("nope");
            }
        });

        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishIntent("like");

            }
        });

        superLikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishIntent("superlike");

            }
        });
    }

    private void initViews() throws JSONException {

        optionsLayout = (LinearLayout) findViewById(R.id.optionsLayout);
        nopeButton = (RelativeLayout) findViewById(R.id.nopeButton);
        likeButton = (RelativeLayout) findViewById(R.id.likeButton);
        superLikeButton = (RelativeLayout) findViewById(R.id.superlikeButton);

    }

    public void finishIntent(String type) {
        Intent intent = new Intent();
        intent.putExtra("swipe", type);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void showSortPopup(View view) {
        PopupWindow popupwindow_obj = popupDisplay();
        popupwindow_obj.showAsDropDown(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupwindow_obj.setElevation(10);
        }
    }

    public void showReportLayout() throws JSONException {
        final Dialog dialog = new Dialog(ProfileActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.report_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        LinearLayout fakeProfileLayout, spamLayout, inappropriateLayout;
        inappropriateLayout = (LinearLayout) dialog.findViewById(R.id.inappropriateLayout);
        spamLayout = (LinearLayout) dialog.findViewById(R.id.spamLayout);
        fakeProfileLayout = (LinearLayout) dialog.findViewById(R.id.fakeProfileLayout);

        fakeProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    reportPerson(getResources().getString(R.string.fake_profile), dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        spamLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    reportPerson(getResources().getString(R.string.feels_like_spam), dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        inappropriateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    reportPerson(getResources().getString(R.string.inappropriate), dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    private void reportPerson(String reason, final Dialog dialog) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("senderID", appSettings.getUserId());
        jsonObject.put("receiverID", jsonObject.optString("id"));
        jsonObject.put("reason", reason);
        ApiCall.PostMethodHeaders(ProfileActivity.this, UrlHelper.REPORT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                intent.putExtra("tab", "1");
                startActivity(intent);
            }
        });
    }

    public PopupWindow popupDisplay() {

        final PopupWindow popupWindow = new PopupWindow(this);

        // inflate your layout or dynamically add view
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.pop_up_menu_layout, null);

//        Button item = (Button) view.findViewById(R.id.button1);

        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.transparent));

        popupWindow.setBackgroundDrawable(colorDrawable);
        popupWindow.setContentView(view);


        return popupWindow;
    }


    private void loadViewForCode() {
        PullToZoomScrollViewEx scrollView = (PullToZoomScrollViewEx) findViewById(R.id.scroll_view);
        View headView = LayoutInflater.from(this).inflate(R.layout.profile_head_view, null, false);
        View zoomView = LayoutInflater.from(this).inflate(R.layout.fragment_view_pager, null, false);
        View contentView = LayoutInflater.from(this).inflate(R.layout.profile_scroll_layout, null, false);
        scrollView.setHeaderView(headView);
        scrollView.setZoomView(zoomView);
        scrollView.setParallax(false);
        scrollView.setScrollContentView(contentView);

        userName = contentView.findViewById(R.id.userName);
        age = contentView.findViewById(R.id.age);
        designation = contentView.findViewById(R.id.designation);
        location = contentView.findViewById(R.id.location);
        about = contentView.findViewById(R.id.about);
        instaviewpager = findViewById(R.id.instaviewpager);
        instagramPhotoCount = findViewById(R.id.instagramPhotoCount);

        workingLayout = (LinearLayout) findViewById(R.id.workingLayout);
        aboutLayout = (LinearLayout) findViewById(R.id.aboutLayout);
        instagramLayout = (LinearLayout) findViewById(R.id.instagramLayout);
        bottomLayout = (LinearLayout) findViewById(R.id.bottomLayout);

        userName.setText(jsonObject.optString("userName"));
        designation.setText(jsonObject.optString("jobTitle") + " " + getString(R.string.at) + " " + jsonObject.optString("company"));
        if (jsonObject.optString("jobTitle").trim().length() == 0) {
            workingLayout.setVisibility(View.GONE);
        } else {
            workingLayout.setVisibility(View.VISIBLE);
        }

        Log.e(TAG, "loadViewForCode: " + jsonObject);

        if (!jsonObject.optString("dontShowAge").equals("1")) {
            age.setText(new StringBuffer().append(", ").append(jsonObject.optString("age")));
        }
        try {
            double km = Double.parseDouble(jsonObject.optString("distance"));
            int miles = (int) (km * 0.621);
            if (!jsonObject.optString("makeDistanceInvisible").equals("1")) {
                location.setText(new StringBuffer().append("").append(miles)
                        .append(" ").append(getString(R.string.miles_away_text)).toString());
            }
        } catch (Exception e) {

        }

        about.setText(jsonObject.optString("aboutUser"));
        if (jsonObject.optString("aboutUser").trim().length() == 0) {
            aboutLayout.setVisibility(View.GONE);
        } else {
            aboutLayout.setVisibility(View.VISIBLE);

        }

        if (getIntent().getStringExtra("shouldShow").equalsIgnoreCase("yes")) {
            bottomLayout.setVisibility(View.VISIBLE);

        } else {
            bottomLayout.setVisibility(View.GONE);

        }


        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
        int mScreenHeight = localDisplayMetrics.heightPixels;
        int mScreenWidth = localDisplayMetrics.widthPixels;
        LinearLayout.LayoutParams localObject = new LinearLayout.LayoutParams(mScreenWidth, (int) (9.0F * (mScreenWidth / 16.0F) + 150));
        scrollView.setHeaderLayoutParams(localObject);

        viewPager = (ViewPager) headView.findViewById(R.id.viewPager);
        TextView reportView = (TextView) contentView.findViewById(R.id.reportView);

        if (jsonObject.optJSONArray("profile_images").length() > 0) {
            backgrounds = new String[jsonObject.optJSONArray("profile_images").length()];
            for (int i = 0; i < backgrounds.length; i++) {
                JSONObject imgObj = jsonObject.optJSONArray("profile_images").optJSONObject(i);
                backgrounds[i] = imgObj.optString("imageURL");
            }
        }
        Log.d(TAG, "loadViewForCode: " + backgrounds.length);

        if (backgrounds.length > 0) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), backgrounds);
            viewPager.setOffscreenPageLimit(1);
            viewPager.setAdapter(adapter);
            reportView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        showReportLayout();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            LinePageIndicator titleIndicator = (LinePageIndicator) findViewById(R.id.indicator);
            int le = mScreenWidth / adapter.getCount();
            titleIndicator.setLineWidth(le);
            titleIndicator.setViewPager(viewPager);
        }
        if (isConnected) {
            getImagesFromInstagram();
        } else {
            instagramLayout.setVisibility(View.GONE);
        }

    }


    private void getImagesFromInstagram() {
        String url = "https://api.instagram.com/v1/users/" + instaId + "/media/recent/?access_token=" + instaToken;
        ApiCall.getMethod(this, url, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray data = response.optJSONArray("data");
                JSONArray instagramImagesArray = new JSONArray();

                for (int i = 0; i < data.length(); i++) {
                    JSONObject singleObject = data.optJSONObject(i);
                    JSONObject imagesObject = singleObject.optJSONObject("images").optJSONObject("standard_resolution");
                    String image = imagesObject.optString("url");
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("image", image);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    instagramImagesArray.put(jsonObject);
                }
                appSettings.setInstaGramPhotos(instagramImagesArray);
                appSettings.setTotalCount("" + data.length());

                JSONArray instaArray = new JSONArray();
                try {
                    instaArray = appSettings.getInstaGramPhotos();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                instaGram = new String[instaArray.length()];
                for (int i = 0; i < instaArray.length(); i++) {
                    JSONObject imgObj = instaArray.optJSONObject(i);
                    if (!imgObj.optString("image").equalsIgnoreCase("")) {

                        instaGram[i] = imgObj.optString("image");

                    }
                }
                instagramPhotoCount.setText(instaGram.length + " " + getResources().getString(R.string.instagram_photos));


                InstaViewPagerAdapter instadapter = new InstaViewPagerAdapter(getSupportFragmentManager());
                instaviewpager.setOffscreenPageLimit(1);
                instaviewpager.setAdapter(instadapter);
                CircleIndicator indicator = (CircleIndicator) findViewById(R.id.instaindicator);
                indicator.setViewPager(instaviewpager);
            }
        });
    }


    public class InstaViewPagerAdapter extends FragmentStatePagerAdapter {


        public InstaViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {


            return InstagramPhotoFragment.newInstance(instaGram, position);
        }

        @Override
        public int getCount() {
            int totalc = 0;
            if ((instaGram.length / 6) == 1) {
                totalc = instaGram.length / 6;

            } else {
                totalc = (instaGram.length / 6) + 1;
            }
            Log.d(TAG, "getCount: " + totalc);
            return totalc;
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
        String[] backgrounds;


        public ViewPagerAdapter(FragmentManager fm, String[] backgrounds) {
            super(fm);
            this.backgrounds = backgrounds;
        }

        @Override
        public Fragment getItem(int position) {
            return ViewPagerFragment.newInstance(backgrounds[position]);
        }

        @Override
        public int getCount() {
            return backgrounds.length;
        }
    }
}
