package com.app.myswipe.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.R;

import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashScreen extends AppCompatActivity {
    ImageView heartIcon;
    AppSettings appSettings = new AppSettings(SplashScreen.this);
    private String TAG = SplashScreen.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        JSONObject jsonObject = new JSONObject();

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
               // String value = getIntent().getExtras().getString(key);
             //   try {
                  //  jsonObject.put(key, value);
              //  } catch (JSONException e) {
               //     e.printStackTrace();
              //  }

            }
            if (jsonObject.length() == 0) {
                initViews();
            } else {
                handleNotification(jsonObject);
            }

        }


    }

    private void handleNotification(JSONObject jsonObject) {
        Intent intent;
        if (jsonObject.optString("notification_type").equalsIgnoreCase("chat")) {
            intent = new Intent(this, ChatActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("receiverId", jsonObject.optString("sender_id"));


        } else if (jsonObject.optString("notification_type").equalsIgnoreCase("match")) {


            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("tab", "1");

        } else {
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("type", "success");
        }
        startActivity(intent);
    }

    private void initViews() {

        if (appSettings.getIsLogged().equalsIgnoreCase("true")) {
            Intent intent = new Intent(SplashScreen.this, MainActivity.class);
            intent.putExtra("tab", "0");
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(SplashScreen.this, SplashActivity.class);
            startActivity(intent);
            finish();

        }
        // heartIcon = (ImageView) findViewById(R.id.heartIcon);
        /*YoYo.with(Techniques.Bounce).duration(1000).onEnd(new YoYo.AnimatorCallback() {
            @Override
            public void call(Animator animator) {

            }
        }).repeat(1).playOn(heartIcon);*/

//
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
