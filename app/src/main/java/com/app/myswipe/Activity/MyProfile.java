package com.app.myswipe.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ecloud.pulltozoomview.PullToZoomScrollViewEx;
import com.app.myswipe.Fragment.InstagramPhotoFragment;
import com.app.myswipe.Fragment.ViewPagerFragment;
import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.Instagram.InstagramSession;
import com.app.myswipe.R;
import com.app.myswipe.Volley.ApiCall;
import com.app.myswipe.Volley.VolleyCallback;
import com.viewpagerindicator.LinePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyProfile extends AppCompatActivity {

    private ViewPager viewPager, instaviewPager;
    String[] backgrounds;
    String[] instaGram;
    LinearLayout optionsLayout;
    int count, addcount;
    CardView editProfile;
    LinearLayout workingLayout, instagramLayout,aboutLayout;

    TextView instagramPhotoCount;
    AppSettings appSettings = new AppSettings(MyProfile.this);

    TextView userName, designation, about, age, location;
    private String TAG = MyProfile.class.getSimpleName();
    private InstagramSession instagramSession;
    private PullToZoomScrollViewEx scrollView;
    private View headView;
    private View zoomView;
    private View contentView;
    private int mScreenWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        instagramSession = new InstagramSession(MyProfile.this);
        initPagers();



    }

    @Override
    protected void onResume() {
        super.onResume();
        count = 0;
        addcount = 0;
        loadViewForCode();
        initViews();
        initListners();
        setData();
    }

    private void initPagers() {
        scrollView = (PullToZoomScrollViewEx) findViewById(R.id.scroll_view);
        headView = LayoutInflater.from(this).inflate(R.layout.profile_head_view, null, false);
        zoomView = LayoutInflater.from(this).inflate(R.layout.fragment_view_pager, null, false);
        contentView = LayoutInflater.from(this).inflate(R.layout.myprofile_scroll_layout, null, false);
        scrollView.setHeaderView(headView);
        scrollView.setZoomView(zoomView);
        scrollView.setParallax(false);
        scrollView.setScrollContentView(contentView);
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
        mScreenWidth = localDisplayMetrics.widthPixels;
        LinearLayout.LayoutParams localObject = new LinearLayout.LayoutParams(mScreenWidth, (int) (9.0F * (mScreenWidth / 16.0F) + 150));
        scrollView.setHeaderLayoutParams(localObject);



        userName = contentView.findViewById(R.id.userName);
        age = contentView.findViewById(R.id.age);
        designation = contentView.findViewById(R.id.designation);
        location = contentView.findViewById(R.id.location);
        about = contentView.findViewById(R.id.about);
        instagramPhotoCount = findViewById(R.id.instagramPhotoCount);
        aboutLayout = findViewById(R.id.aboutLayout);

        workingLayout = (LinearLayout) findViewById(R.id.workingLayout);
        instagramLayout = (LinearLayout) findViewById(R.id.instagramLayout);


        instaviewPager = (ViewPager) findViewById(R.id.instaviewpager);


        if (appSettings.getJobTitle().trim().length() == 0) {
            workingLayout.setVisibility(View.GONE);
        } else {
            workingLayout.setVisibility(View.VISIBLE);
        }



        if (appSettings.getIsInstagramConnected().equalsIgnoreCase("true")) {
            getImagesFromInstagram();
        } else {
            instagramLayout.setVisibility(View.GONE);
        }


    }

    private void setData() {

        userName.setText(appSettings.getUserName());
        age.setText(appSettings.getAge());
        designation.setText(appSettings.getJobTitle() + " "+getString(R.string.at)+" " + appSettings.getCompany());
        location.setText(getString(R.string.miles_away));
        about.setText(appSettings.getAboutUs());
        if (appSettings.getAboutUs().trim().length()==0)
        {
            aboutLayout.setVisibility(View.GONE);
        } else {
            aboutLayout.setVisibility(View.VISIBLE);

        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setData();

    }

    private void initListners() {
        optionsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSortPopup(view);
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyProfile.this, EditProfileActivity.class);
                startActivityForResult(intent, 10);
            }
        });
    }

    private void initViews() {

        optionsLayout = (LinearLayout) findViewById(R.id.optionsLayout);
        editProfile = (CardView) findViewById(R.id.editProfile);
    }

    private void showSortPopup(View view) {
        PopupWindow popupwindow_obj = popupDisplay();
        popupwindow_obj.showAsDropDown(view);// where u want show on view click event popupwindow.showAsDropDown(view, x, y);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupwindow_obj.setElevation(30);
        }


    }

    public void showReportLayout() {
        Dialog dialog = new Dialog(MyProfile.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.report_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }


    private void getImagesFromInstagram() {
        String url = "https://api.instagram.com/v1/users/" + instagramSession.getId() + "/media/recent/?access_token=" + instagramSession.getAccessToken();
        ApiCall.getMethod(MyProfile.this, url, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray data = response.optJSONArray("data");
                JSONArray instagramImagesArray = new JSONArray();

                for (int i = 0; i < data.length(); i++) {
                    JSONObject singleObject = data.optJSONObject(i);
                    JSONObject imagesObject = singleObject.optJSONObject("images").optJSONObject("standard_resolution");
                    String image = imagesObject.optString("url");
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("image", image);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    instagramImagesArray.put(jsonObject);
                }
                appSettings.setInstaGramPhotos(instagramImagesArray);
                appSettings.setTotalCount("" + data.length());

                JSONArray instaArray = new JSONArray();
                try {
                    instaArray = appSettings.getInstaGramPhotos();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                instaGram = new String[instaArray.length()];
                for (int i = 0; i < instaArray.length(); i++) {
                    JSONObject imgObj = instaArray.optJSONObject(i);
                    if (!imgObj.optString("image").equalsIgnoreCase("")) {

                        instaGram[i] = imgObj.optString("image");

                    }
                }

                instagramPhotoCount.setText(instaGram.length + " " + getResources().getString(R.string.instagram_photos));


                InstaViewPagerAdapter instadapter = new InstaViewPagerAdapter(getSupportFragmentManager());
                instaviewPager.setOffscreenPageLimit(1);
                instaviewPager.setAdapter(instadapter);
                CircleIndicator indicator = (CircleIndicator) findViewById(R.id.instaindicator);
                indicator.setViewPager(instaviewPager);
            }
        });
    }

    public PopupWindow popupDisplay() {

        final PopupWindow popupWindow = new PopupWindow(this);

        // inflate your layout or dynamically add view
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.pop_up_menu_layout, null);

//        Button item = (Button) view.findViewById(R.id.button1);

        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.transparent));

        popupWindow.setBackgroundDrawable(colorDrawable);
        popupWindow.setContentView(view);

        return popupWindow;
    }


    private void loadViewForCode() {
        viewPager = (ViewPager) headView.findViewById(R.id.viewPager);

        backgrounds=new String[0];



        JSONArray jsonArray = appSettings.getImagesArray();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject imgObj = jsonArray.optJSONObject(i);
            if (!imgObj.optString("imageURL").equalsIgnoreCase("")) {
                count = count + 1;
            }
        }

        backgrounds = new String[count];
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject imgObj = jsonArray.optJSONObject(i);
            if (!imgObj.optString("imageURL").equalsIgnoreCase("")) {
                if (addcount < count) {
                    backgrounds[addcount] = imgObj.optString("imageURL");
                    addcount = addcount + 1;
                }
            }
        }


        if (backgrounds.length > 0) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPager.setOffscreenPageLimit(1);
            viewPager.setAdapter(adapter);
            LinePageIndicator titleIndicator = (LinePageIndicator) findViewById(R.id.indicator);
            int le = mScreenWidth / adapter.getCount();
            titleIndicator.setLineWidth(le);
            titleIndicator.setViewPager(viewPager);

        }




    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {


        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ViewPagerFragment.newInstance(backgrounds[position]);
        }

        @Override
        public int getCount() {
            return backgrounds.length;
        }
    }


    public class InstaViewPagerAdapter extends FragmentStatePagerAdapter {


        public InstaViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {


            return InstagramPhotoFragment.newInstance(instaGram, position);
        }

        @Override
        public int getCount() {
            int totalc = 0;
            if ((instaGram.length / 6) == 1) {
                totalc = instaGram.length / 6;

            } else {
                totalc = (instaGram.length / 6) + 1;
            }
            Log.d(TAG, "getCount: " + totalc);
            return totalc;
        }
    }


}
