package com.app.myswipe.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.techisfun.android.topsheet.TopSheetBehavior;
import com.luolc.emojirain.EmojiRainLayout;
import com.app.myswipe.Adapters.ChatsAdapter;
import com.app.myswipe.Adapters.EmojiAdapter;
import com.app.myswipe.Fragment.EmojiFragment;
import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.Helper.UrlHelper;
import com.app.myswipe.Helper.Utils;
import com.app.myswipe.R;
import com.app.myswipe.Services.EmojiMessageEvent;
import com.app.myswipe.Services.MessageEvent;
import com.app.myswipe.Services.ServiceClass;
import com.app.myswipe.Volley.ApiCall;
import com.app.myswipe.Volley.VolleyCallback;
import com.viewpagerindicator.IconPageIndicator;
import com.viewpagerindicator.IconPagerAdapter;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView messageItems;
    LinearLayout smileyLayout;
    JSONArray messageArray = new JSONArray();
    JSONArray userdetails = new JSONArray();
    TextView userName;
    CircleImageView userImage;
    ImageView backIcon;
    LinearLayout sendLayout;
    JSONObject jsonObject = new JSONObject();
    String receiverId;
    String senderId;
    EditText messageInput;
    AppSettings appSettings = new AppSettings(ChatActivity.this);
    ServiceClass.Emitters emitters = new ServiceClass.Emitters(ChatActivity.this);
    private ChatsAdapter chatsAdapter;
    public static String displayPic;
    ImageView optionsButton;
    CardView messageCard;
    private LinearLayoutManager linearLayoutManager;
    public static EmojiRainLayout mContainer;
    public static View sheet;
    public static boolean isExpanded = false;
    public static boolean noAnimation = true;
    public static ViewPager viewPager;
    CardView inLayCard;
    LinearLayout userDetails;
    View viewHolder;
    private DiscreteScrollView emojiRecyclerView;
    private static final int[] ICONS = new int[]{
            R.drawable.heart_sel_indicator,
            R.drawable.laugh_grey,
            R.drawable.wow_grey,
            R.drawable.angry_grey,
    };
    private static final String[] CONTENT = new String[]{"Calendar", "Camera", "Alarms", "Location"};
    private String TAG = ChatActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        initViews();
        initListners();
        setValues();
        initBottomSheet();
        initIndicator();


    }

    private void initIndicator() {

        IconPageIndicator indicator = (IconPageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);


        if (Build.VERSION.SDK_INT >= 11) {
            messageItems.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v,
                                           int left, int top, int right, int bottom,
                                           int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    if (bottom < oldBottom) {
                        messageItems.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    messageItems.smoothScrollToPosition(
                                            messageItems.getAdapter().getItemCount() - 1);
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        }, 100);
                    }
                }
            });
        }


    }

    private void initBottomSheet() {
//        View bottomSheet = findViewById(R.id.design_bottom_sheet);
        viewPager = findViewById(R.id.emojiViewPager);
//        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
//        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                switch (newState) {
//                    case BottomSheetBehavior.STATE_DRAGGING:
//                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
//                        break;
//                    case BottomSheetBehavior.STATE_SETTLING:
//                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
//                        break;
//                    case BottomSheetBehavior.STATE_EXPANDED:
//                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
//                        break;
//                    case BottomSheetBehavior.STATE_COLLAPSED:
//                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
//                        break;
//                    case BottomSheetBehavior.STATE_HIDDEN:
//                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
//                        break;
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
//            }
//        });
//
//

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(adapter);
        sheet = findViewById(R.id.top_sheet);
        TopSheetBehavior.from(sheet).setState(TopSheetBehavior.STATE_EXPANDED);


        emojiRecyclerView = (DiscreteScrollView) findViewById(R.id.emojiRecyclerView);
        emojiRecyclerView.setItemTransformer(new ScaleTransformer.Builder()
                .setMaxScale(1.1f)
                .setMinScale(0.9f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                .build());
        EmojiAdapter emoadapter = new EmojiAdapter(ChatActivity.this);
        emojiRecyclerView.setAdapter(emoadapter);
        emojiRecyclerView.setVisibility(View.GONE);
        emojiRecyclerView.addOnItemChangedListener(new DiscreteScrollView.OnItemChangedListener<RecyclerView.ViewHolder>() {
            @Override
            public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {
                viewPager.setCurrentItem(adapterPosition);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int adapterPosition, float positionOffset, int positionOffsetPixels) {
                emojiRecyclerView.scrollToPosition(adapterPosition);
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TopSheetBehavior.from(sheet).setTopSheetCallback(new TopSheetBehavior.TopSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == TopSheetBehavior.STATE_DRAGGING) {
                    isExpanded = false;
                    Utils.hideKeyboard(ChatActivity.this);
                } else if (newState == TopSheetBehavior.STATE_EXPANDED) {
                    isExpanded = true;
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.d("onSlide: ", "slides" + slideOffset);
                if (slideOffset == 1) {
                    setValuesCard(0);
                    isExpanded = true;

                } else if (slideOffset > 0.9) {
                    inLayCard.setVisibility(View.VISIBLE);


                    setValuesCard(3);
                    isExpanded = true;


                } else if (slideOffset > 0.8) {
                    setValuesCard(5);
                    isExpanded = false;


                } else if (slideOffset > 0.7) {
                    setValuesCard(7);
                    isExpanded = false;

                } else if (slideOffset > 0.6) {
                    setValuesCard(9);
                    isExpanded = false;

                } else if (slideOffset > 0.5) {
                    inLayCard.setVisibility(View.GONE);
                    setValuesCard(11);
                    isExpanded = false;

                } else if (slideOffset > 0.4) {
                    setValuesCard(13);

                    isExpanded = false;

                } else if (slideOffset > 0.3) {
                    setValuesCard(15);
                    isExpanded = false;

                } else if (slideOffset > 0.2) {
                    setValuesCard(17);
                    isExpanded = false;

                } else if (slideOffset > 0.1) {
                    setValuesCard(19);
                    isExpanded = false;

                } else if (slideOffset == 0) {
                    setValuesCard(25);
                    isExpanded = true;

                } else if (slideOffset < 0.1 && slideOffset > 0) {
                    setValuesCard(25);
                    isExpanded = false;

                }

            }
        });


    }

    private void setValuesCard(int i) {

        if (i == 0 || i == 3) {
            messageCard.setRadius(i);
            messageCard.setCardElevation(0);

            ViewGroup.MarginLayoutParams layoutParams =
                    (ViewGroup.MarginLayoutParams) messageCard.getLayoutParams();
            layoutParams.setMargins(convertDpToPixel(i), 0, convertDpToPixel(i), 0);


            messageCard.requestLayout();
//            if (i == 0) {
            chatsAdapter.refresh();
//            }


        } else {
            messageCard.setRadius(i);
            messageCard.setCardElevation(15);
            ViewGroup.MarginLayoutParams layoutParams =
                    (ViewGroup.MarginLayoutParams) messageCard.getLayoutParams();
            layoutParams.setMargins(convertDpToPixel(i), 0, convertDpToPixel(i), 30);
            messageCard.requestLayout();

        }
    }

    public int convertDpToPixel(float dp) {
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

//    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
//
//
//        public ViewPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            position = position + 1;
//            return EmojiFragment.newInstance("" + position);
//        }
//
//        @Override
//        public int getCount() {
//            return 4;
//        }
//    }
//


    public class ViewPagerAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return EmojiFragment.newInstance("" + position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length].toUpperCase();
        }

        @Override
        public int getIconResId(int index) {
            return ICONS[index];
        }

        @Override
        public int getCount() {
            return 4;
        }
    }


    private void setValues() {
        receiverId = getIntent().getStringExtra("receiverId");

        try {

            getData();

        } catch (Exception e) {
            e.printStackTrace();
        }

//        try {
//            getDummyDatas();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


    }

    private void initListners() {
        backIcon.setOnClickListener(this);
        sendLayout.setOnClickListener(this);
        optionsButton.setOnClickListener(this);
        smileyLayout.setOnClickListener(this);
        userDetails.setOnClickListener(this);

        messageItems.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (linearLayoutManager.findLastVisibleItemPosition() == chatsAdapter.getItemCount() - 1) {
                        viewHolder.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.setVisibility(View.GONE);
                    }
                }

                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    if (linearLayoutManager.findLastVisibleItemPosition() == chatsAdapter.getItemCount() - 1) {
                        viewHolder.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void initViews() {
        messageItems = (RecyclerView) findViewById(R.id.messageItems);
        userName = (TextView) findViewById(R.id.userName);
        userImage = (CircleImageView) findViewById(R.id.userImage);
        sendLayout = (LinearLayout) findViewById(R.id.sendLayout);
        userDetails = (LinearLayout) findViewById(R.id.userDetails);
        smileyLayout = (LinearLayout) findViewById(R.id.smileyLayout);
        inLayCard = (CardView) findViewById(R.id.inLay);
        backIcon = (ImageView) findViewById(R.id.backIcon);
        messageInput = (EditText) findViewById(R.id.messageInput);
        optionsButton = (ImageView) findViewById(R.id.optionsButton);
        messageCard = (CardView) findViewById(R.id.messageCard);
        mContainer = (EmojiRainLayout) findViewById(R.id.group_emoji_container);
        viewHolder = (View) findViewById(R.id.viewHolder);

    }


    public static void startDropping(String position) {
        mContainer.clearEmojis();
        mContainer.stopDropping();

        TopSheetBehavior.from(sheet).setState(TopSheetBehavior.STATE_EXPANDED);


        if (noAnimation) {

            if (isExpanded) {
                noAnimation = false;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        noAnimation = true;
                    }
                }, 4000);
                String positon = position;
                if (positon.equalsIgnoreCase("0")) {
                    mContainer.clearEmojis();
                    mContainer.addEmoji(R.drawable.ic_heart);

                } else if (positon.equalsIgnoreCase("1")) {
                    mContainer.clearEmojis();
                    mContainer.addEmoji(R.drawable.ic_laughing);

                } else if (positon.equalsIgnoreCase("2")) {
                    mContainer.clearEmojis();
                    mContainer.addEmoji(R.drawable.ic_surprised);
                } else {
                    mContainer.clearEmojis();
                    mContainer.addEmoji(R.drawable.ic_angry);
                }
                viewPager.setCurrentItem(0);
                mContainer.startDropping();
            }
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

//    public void getDummyDatas() throws JSONException {
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("type", "sender");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "receiver");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "sender");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "receiver");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "receiver");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "receiver");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "sender");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "receiver");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "receiver");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "receiver");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "sender");
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("content_type", "text");
//
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "receiver");
//        jsonObject.put("content_type", "text");
//
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        jsonObject.put("type", "receiver");
//        jsonObject.put("content_type", "text");
//
//        jsonObject.put("content", "asdasd");
//        jsonObject.put("Time", System.currentTimeMillis());
//        messageArray.put(jsonObject);
//        jsonObject = new JSONObject();
//        chatsAdapter = new ChatsAdapter(messageArray, ChatActivity.this, userName.getText().toString());
//        linearLayoutManager = new LinearLayoutManager(ChatActivity.this, LinearLayoutManager.VERTICAL, false);
//        messageItems.setLayoutManager(linearLayoutManager);
//        messageItems.setAdapter(chatsAdapter);
//        chatsAdapter.notifyDataSetChanged();
//        linearLayoutManager.scrollToPosition(messageArray.length() - 1);
//
//    }

    private void getData() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("senderID", appSettings.getUserId());
        jsonObject.put("receiverID", receiverId);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodHeaders(ChatActivity.this, UrlHelper.CHAT_LISTS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);
                JSONArray results = response.optJSONArray("results");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject jsonObjects = results.optJSONObject(i);
                    if (jsonObjects.optString("senderID").equalsIgnoreCase(receiverId)) {
                        try {
                            jsonObjects.put("type", "receiver");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            jsonObjects.put("type", "sender");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    messageArray.put(jsonObjects);
                }
                userdetails = response.optJSONArray("userdetails");

                Log.d(TAG, "fiinalArray " + messageArray);
                chatsAdapter = new ChatsAdapter(messageArray, ChatActivity.this, jsonObject.optString("userName"));
                linearLayoutManager = new LinearLayoutManager(ChatActivity.this, LinearLayoutManager.VERTICAL, false);
                messageItems.setLayoutManager(linearLayoutManager);
                messageItems.setAdapter(chatsAdapter);
                chatsAdapter.notifyDataSetChanged();
                linearLayoutManager.scrollToPosition(messageArray.length() - 1);

                displayPic = userdetails.optJSONObject(0).optString("displayPic");
                userName.setText(userdetails.optJSONObject(0).optString("userName"));
                Glide.with(ChatActivity.this).load(displayPic).into(userImage);
                senderId = appSettings.getUserId();


            }
        });

    }


    private void showSortPopup(View view) {
        PopupWindow popupwindow_obj = popupDisplay();
        popupwindow_obj.showAsDropDown(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupwindow_obj.setElevation(10);
        }
    }


    public PopupWindow popupDisplay() {

        final PopupWindow popupWindow = new PopupWindow(this);

        // inflate your layout or dynamically add view
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.pop_up_menu_chat, null);

//        Button item = (Button) view.findViewById(R.id.button1);

        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.transparent));

        popupWindow.setBackgroundDrawable(colorDrawable);
        popupWindow.setContentView(view);
        TextView unMatch = view.findViewById(R.id.unMatch);
        TextView reportLay = view.findViewById(R.id.reportLay);
        unMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    unmatch();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        reportLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    showReportLayout();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return popupWindow;
    }


    public void showReportLayout() throws JSONException {
        final Dialog dialog = new Dialog(ChatActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.report_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        LinearLayout fakeProfileLayout, spamLayout, inappropriateLayout;
        inappropriateLayout = (LinearLayout) dialog.findViewById(R.id.inappropriateLayout);
        spamLayout = (LinearLayout) dialog.findViewById(R.id.spamLayout);
        fakeProfileLayout = (LinearLayout) dialog.findViewById(R.id.fakeProfileLayout);

        fakeProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    reportPerson(getResources().getString(R.string.fake_profile), dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        spamLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    reportPerson(getResources().getString(R.string.feels_like_spam), dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        inappropriateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    reportPerson(getResources().getString(R.string.inappropriate), dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    private void reportPerson(String reason, final Dialog dialog) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("senderID", appSettings.getUserId());
        jsonObject.put("receiverID", receiverId);
        jsonObject.put("reason", reason);
        ApiCall.PostMethodHeaders(ChatActivity.this, UrlHelper.REPORT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                Intent intent = new Intent(ChatActivity.this, MainActivity.class);
                intent.putExtra("tab", "1");
                startActivity(intent);
            }
        });
    }

    private void unmatch() throws JSONException {
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("accessToken", appSettings.getAccessToken());
        jsonObject1.put("unmatchID", receiverId);
        ApiCall.PostMethodHeaders(ChatActivity.this, UrlHelper.UNMATCH, jsonObject1, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Intent intent = new Intent(ChatActivity.this, MainActivity.class);
                intent.putExtra("tab", "1");
                startActivity(intent);
            }
        });
    }


    public void updateMessages(JSONObject jsonObject) {
        chatsAdapter.addMessage(jsonObject);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {

        if (event.getJsonObject().optString("sender_id").equalsIgnoreCase(receiverId)) {
            isExpanded = true;
            if (event.getJsonObject().optString("content_type").equalsIgnoreCase("heart")) {
                startDropping("0");

            }
            if (event.getJsonObject().optString("content_type").equalsIgnoreCase("laugh")) {
                startDropping("1");

            }
            if (event.getJsonObject().optString("content_type").equalsIgnoreCase("wow")) {
                startDropping("2");

            }
            if (event.getJsonObject().optString("content_type").equalsIgnoreCase("angry")) {
                startDropping("3");


            }
            updateMessages(event.getJsonObject());
            linearLayoutManager.scrollToPosition(messageArray.length() - 1);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnEmojiMessageEvent(EmojiMessageEvent event) {


        sendEmoji(event.getType());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void sendEmoji(String type) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("sender_id", senderId);
            jsonObject.put("reciever_id", receiverId);
            jsonObject.put("fromName", appSettings.getUserName());

            jsonObject.put("message_id", UUID.randomUUID());
            jsonObject.put("Time", System.currentTimeMillis());
//            if (type.equalsIgnoreCase("heart")) {
                jsonObject.put("content", userName.getText().toString() + getResources().getString(R.string.sent_a));
//            } else if (type.equalsIgnoreCase("wow")) {
//                jsonObject.put("content", userName.getText().toString() + getResources().getString(R.string.sent_a));
//            } else if (type.equalsIgnoreCase("angry")) {
//                jsonObject.put("content", userName.getText().toString() + getResources().getString(R.string.sent_a));
//            }else if (type.equalsIgnoreCase("laugh")) {
//                jsonObject.put("content", userName.getText().toString() + getResources().getString(R.string.sent_a));
//            }
            jsonObject.put("content_type", type);
            jsonObject.put("type", "sender");
            updateMessages(jsonObject);
            emitters.sendMessage(jsonObject);
        } catch (Exception e) {

        }
        linearLayoutManager.scrollToPosition(messageArray.length() - 1);

    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra("type").equalsIgnoreCase("click")) {
            finish();
        } else {
            Intent intent;
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("tab", "1");
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == backIcon) {


            onBackPressed();

        } else if (view == sendLayout) {
            if (messageInput.getText().toString().trim().length() != 0) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("sender_id", senderId);
                    jsonObject.put("reciever_id", receiverId);
                    jsonObject.put("message_id", UUID.randomUUID());
                    jsonObject.put("Time", System.currentTimeMillis());
                    jsonObject.put("content", messageInput.getText().toString());
                    jsonObject.put("content_type", "text");
                    jsonObject.put("type", "sender");
                    jsonObject.put("fromName", appSettings.getUserName());


                    updateMessages(jsonObject);
                    emitters.sendMessage(jsonObject);

                } catch (Exception e) {

                }
                messageInput.setText("");
                linearLayoutManager.scrollToPosition(messageArray.length() - 1);
            }
        } else if (view == optionsButton) {
            showSortPopup(optionsButton);
        } else if (view == smileyLayout) {
            TopSheetBehavior.from(sheet).setState(TopSheetBehavior.STATE_COLLAPSED);

        } else if (view == userDetails) {
            Intent intent = new Intent(ChatActivity.this, ProfileActivity.class);
            intent.putExtra("profileDetails", userdetails.optJSONObject(0).toString());
            intent.putExtra("shouldShow","no");
            startActivityForResult(intent, 121);

        }
    }
}
