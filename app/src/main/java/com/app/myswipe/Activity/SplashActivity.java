package com.app.myswipe.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.app.myswipe.Fragment.TitlePagerFragment;
import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.Helper.UrlHelper;
import com.app.myswipe.R;
import com.app.myswipe.Volley.ApiCall;
import com.app.myswipe.Volley.VolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {
    ViewPager viewPager;
    int currentitem;
    String[] titles;
    String[] backgrounds = new String[]{"http://104.131.74.144/images/background.jpg", "http://104.131.74.144/images/background_phone_number.jpg", "http://104.131.74.144/images/background.jpg", "http://104.131.74.144/images/background_phone_number.jpg"};
    LoginButton loginFbButton;
    LoginManager loginManager;
    Button phoneNumber, facebookButton;
    ImageView backgroundImage;
    CallbackManager callbackManager;
    private String TAG = SplashActivity.class.getSimpleName();
    private int RC_SIGN_IN = 0;
    AppSettings appSettings = new AppSettings(SplashActivity.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        titles = new String[]{getString(R.string.text_one), getString(R.string.text_two), getString(R.string.three), getString(R.string.text_four)};
        FacebookSdk.sdkInitialize(getApplicationContext());

        initViews();
        initListners();
        initFacebookLogin();
        generateKeyHash();
//        Utils.show(SplashActivity.this);
    }

    private void generateKeyHash() {


        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        loginFbButton = (LoginButton) findViewById(R.id.loginFbButton);
        loginManager = LoginManager.getInstance();
//        LoginManager.getInstance()
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                String accessToken = loginResult.getAccessToken().getToken();
                Log.e(TAG, "Access token: " + accessToken);


                get_data_for_facebook(loginResult);
            }

            @Override
            public void onCancel() {
                // App code
                Log.e(TAG, "onCancel: " + "canceling the facebook");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e(TAG, "onError: " + exception);
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }


    }

    private void get_data_for_facebook(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.e(TAG, "JSONObject :" + object);
                Log.e(TAG, "Facebook_Response :" + response);
                JSONObject fbJsonObject = new JSONObject();

                try {

                    final String email_json;
                    String gender = " ";
                    final String first_name = object.optString("first_name");
                    final String last_name = object.optString("last_name");
                    final String id = object.optString("id");
                    String imageUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
                    String sbirthday = "";


                    if (object.has("email")) {
                        email_json = object.optString("email");
                    } else {
                        showAlerDialog();
                        return;
                    }
                   /* if (object.has("gender")) {
                        gender = object.optString("gender");
                    } else {

                        // showAlerDialog();
                       // return;
                    }*/

                    if (object.has("birthday")) {
                        final String birthday = object.optString("birthday");
                        sbirthday = formatString(birthday);
                    } else {

                        showAlerDialog();
                        return;

                    }

                    //object.optString("gender")
                    fbJsonObject.put("email", email_json);
                    fbJsonObject.put("imageURL", imageUrl);
                    fbJsonObject.put("dob", sbirthday);
                    fbJsonObject.put("gender", "Male");
                    fbJsonObject.put("userName", first_name + " " + last_name);
                    fbJsonObject.put("fbToken", id);

                    appSettings.setFbDetails(fbJsonObject.toString());

                    socialLogin(id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email, gender, age_range, locale, birthday ");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void showAlerDialog() {


        final Dialog dialog = new Dialog(SplashActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.missing_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView confirmButton = (TextView) dialog.findViewById(R.id.confirmButton);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginManager.logOut();
                dialog.dismiss();
            }
        });
    }


    private String formatString(String birthday) {
        String[] month = birthday.split("/");
        String monthVal = month[0];
        String dateval = month[1];
        String yearval = month[2];
        String f_val = yearval + "-" + monthVal + "-" + dateval;
        return f_val;
    }

    private void socialLogin(String fbToken) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fbToken", fbToken);
        ApiCall.PostMethod(SplashActivity.this, UrlHelper.FB_CHECK, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                if (response.optString("newUser").equalsIgnoreCase("yes")) {
                    movePhoneNumberActivity("fb");
                } else {
                    appSettings.setAccessToken(response.optString("accessToken"));
                    try {
                        updateToken();
                        getProfileData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void updateToken() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("deviceToken", appSettings.getFireBaseToken());
        jsonObject.put("os", "android");
        ApiCall.PostMethodHeaders(SplashActivity.this, UrlHelper.DEVICE_TOKEN, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

            }
        });
    }


    private void getProfileData() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodHeaders(SplashActivity.this, UrlHelper.VIEW_PROFILE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray jsonArray = response.optJSONArray("results");
                JSONObject object = jsonArray.optJSONObject(0);
                appSettings.setIsLogged("true");
                appSettings.setUserId(object.optString("id"));
                if (object.optJSONArray("images").length() > 0) {
                    appSettings.setUserImageUrl(object.optJSONArray("images").optJSONObject(0).optString("imageURL"));
                }
                appSettings.setAboutUs(object.optString("aboutUser"));
                appSettings.setJobTitle(object.optString("jobTitle"));
                appSettings.setCompany(object.optString("company"));
                appSettings.setUserName(object.optString("userName"));
                appSettings.setCollege(object.optString("school"));
                appSettings.setImagesArray(object.optJSONArray("images"));
                appSettings.setGender(object.optString("gender"));
                appSettings.setAge(object.optString("age"));
                appSettings.setShowAge(object.optString("dontShowAge"));
                appSettings.setShowDistance(object.optString("makeDistanceInvisible"));
                if (object.optString("InstaStatus").equalsIgnoreCase("connect")) {
                    appSettings.setIsInstagramConnected("true");
                    appSettings.setInstagramName(object.optString("InstaName"));
                    appSettings.setInstagramToken(object.optString("InstaToken"));
                    appSettings.setInstagramId(object.optString("InstaSenderID"));
                } else {
                    appSettings.setInstagramName("");
                    appSettings.setTotalCount("0");
                    appSettings.setIsInstagramConnected("false");
                    JSONArray jsonArrays = new JSONArray();
                    appSettings.setInstaGramPhotos(jsonArrays);
                }

                //  appSettings.setUserType("basic");
                moveMainActivity();


            }
        });
    }


    private void initListners() {
        phoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movePhoneNumberActivity("normal");
            }
        });
        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//               moveMainActivity();
                loginManager.logInWithReadPermissions(SplashActivity.this, Arrays.asList("public_profile", "user_birthday", "email"));

            }
        });
    }

    private void movePhoneNumberActivity(String fb) {
        Intent intent = new Intent(SplashActivity.this, PhoneNumberActivity.class);
        appSettings.setLoginType(fb);
        startActivity(intent);

    }

    private void moveMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.putExtra("tab", "0");

        startActivity(intent);
    }

    private void initViews() {
        phoneNumber = (Button) findViewById(R.id.phoneNumber);
        facebookButton = (Button) findViewById(R.id.facebookButton);
        backgroundImage = (ImageView) findViewById(R.id.backgroundImage);


        String[] titles = new String[]{getResources().getString(R.string.swipe_around_the), getResources().getString(R.string.get_free), getResources().getString(R.string.choose_who_sees_you), getResources().getString(R.string.control_your_profile), getResources().getString(R.string.unlimited_likes), getResources().getString(R.string.unlimited_rewinds), getResources().getString(R.string.turn_off)};
        String[] subtitles = new String[]{getResources().getString(R.string.passport_to), getResources().getString(R.string.skip_the_line), getResources().getString(R.string.only_be_shown), getResources().getString(R.string.limit_what_others), getResources().getString(R.string.swipe_right_as), getResources().getString(R.string.go_back_and), getResources().getString(R.string.have_fun)};

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(adapter);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (currentitem < 5) {
                    currentitem = currentitem + 1;
                } else {
                    currentitem = 0;
                }
                viewPager.setCurrentItem(currentitem);
                handler.postDelayed(this, 3000);

            }
        }, 3000);

//        Glide.with(SplashActivity.this).load("http://104.131.74.144/images/background.jpg").into(backgroundImage);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {


        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TitlePagerFragment.newInstance(titles[position], backgrounds[position], position);
        }

        @Override
        public int getCount() {
            return titles.length;
        }
    }
}
