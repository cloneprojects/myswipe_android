package com.app.myswipe.Volley;

import android.content.Context;
import android.content.Intent;

import com.android.volley.VolleyError;
import com.app.myswipe.Activity.SplashActivity;
import com.app.myswipe.Application.AppController;
import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.Helper.UrlHelper;
import com.app.myswipe.Helper.Utils;
import com.app.myswipe.R;

/**
 * Created by user on 19-09-2017.
 */

public class VolleyErrorHandler {


    private static String TAG=VolleyErrorHandler.class.getSimpleName();

    public static void handle(String url, VolleyError volleyError)
    {
        Context context= AppController.getContext();

        if (!url.equalsIgnoreCase(UrlHelper.MAIN_SCREEN)) {


            try {

                Utils.log(TAG, "handle:url " + volleyError.networkResponse.statusCode);

                switch (volleyError.networkResponse.statusCode) {
                    case 500:
                        Utils.toast(context, context.getResources().getString(R.string.some_thing_went_wrong));
                        break;
                    case 401:
                        moveToSignActivity(context);
                        Utils.toast(context, context.getResources().getString(R.string.please_login_again));
                        break;

                }
            } catch (Exception e) {
                Utils.toast(context, context.getResources().getString(R.string.some_thing_went_wrong));

                e.printStackTrace();
            }
        }
    }

    private static void moveToSignActivity(Context context) {
        Intent intent=new Intent(context, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        AppSettings appSettings=new AppSettings(context);
        appSettings.setIsLogged("false");
        context.startActivity(intent);
    }

}
