package com.app.myswipe.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.app.myswipe.R;

import java.util.Arrays;


public class InstagramPhotoFragment extends Fragment {
    ImageView firstImage,secondImage,thirdImage,fourthImage,fifthImage,sixthImage;
    int currnetPost,totalCount;
    private String TAG=InstagramPhotoFragment.class.getSimpleName();
    CardView firstCard,secondCard,thirdCard,fourthCard,fifthCard,sixthCard;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.insta_photo_fragment, container, false);
        firstImage=(ImageView)v.findViewById(R.id.firstImage);
        secondImage=(ImageView)v.findViewById(R.id.secondImage);
        thirdImage=(ImageView)v.findViewById(R.id.thirdImage);
        fourthImage=(ImageView)v.findViewById(R.id.fourthImage);
        fifthImage=(ImageView)v.findViewById(R.id.fifthImage);
        sixthImage=(ImageView)v.findViewById(R.id.sixthImage); 
        
        
        firstCard=(CardView) v.findViewById(R.id.firstCard);
        secondCard=(CardView)v.findViewById(R.id.secondCard);
        thirdCard=(CardView)v.findViewById(R.id.thirdCard);
        fourthCard=(CardView)v.findViewById(R.id.fourthCard);
        fifthCard=(CardView)v.findViewById(R.id.fifthCard);
        sixthCard=(CardView)v.findViewById(R.id.sixthCard);
//        pageText.setText(getArguments().getString("title"));

        String[] totalarray=getArguments().getStringArray("image");
        totalCount=totalarray.length;
        currnetPost=getArguments().getInt("position");

        int endCount;

        int startCount;
        if (currnetPost==0)
        {
            startCount=0;
            endCount=6;
        } else {
            endCount=((currnetPost+1)*6);
            startCount=(currnetPost*6);
        }
        Log.d(TAG, "onCreateView: "+currnetPost+"/"+startCount+"/"+endCount);

        String[] array=new String[6];


            array=Arrays.copyOfRange(totalarray,startCount,endCount);
            for (int i=0;i<array.length;i++) {
                Log.d(TAG, "onCreateView: " + array[i]);
            }


        try {
            Glide.with(getActivity()).load(array[0]).into(firstImage);
        }catch (Exception e)
        {
            Glide.with(getActivity()).load("").into(firstImage);
            firstImage.setVisibility(View.INVISIBLE);

        }
        try {
            Glide.with(getActivity()).load(array[1]).into(secondImage);
        }catch (Exception e)
        {            Glide.with(getActivity()).load("").into(secondImage);
            secondImage.setVisibility(View.INVISIBLE);



        }
        try {
            Glide.with(getActivity()).load(array[2]).into(thirdImage);


        }catch (Exception e)
        {
            Glide.with(getActivity()).load("").into(thirdImage);
            thirdImage.setVisibility(View.INVISIBLE);



        }
        try {
            Glide.with(getActivity()).load(array[3]).into(fourthImage);
        }catch (Exception e)
        {
            Glide.with(getActivity()).load("").into(fourthImage);
            fourthImage.setVisibility(View.INVISIBLE);


        }
        try {
            Glide.with(getActivity()).load(array[4]).into(fifthImage);
        }catch (Exception e)
        {
            Glide.with(getActivity()).load("").into(fifthImage);
            fifthImage.setVisibility(View.INVISIBLE);



        }
        try {
            Glide.with(getActivity()).load(array[5]).into(sixthImage);
        }catch (Exception e)
        {
            Glide.with(getActivity()).load("").into(sixthImage);
            sixthImage.setVisibility(View.INVISIBLE);


        }
        try {
            if (array[0].trim().length() == 0) {
                firstCard.setVisibility(View.INVISIBLE);
            }
        }catch (Exception e)
        {
            firstCard.setVisibility(View.INVISIBLE);


        }
        try {
            if (array[1].trim().length() == 0) {
                secondCard.setVisibility(View.INVISIBLE);
            }
        }catch (Exception e)
        {
            secondCard.setVisibility(View.INVISIBLE);

        }
        try{
        if (array[2].trim().length()==0)
        {
            thirdCard.setVisibility(View.INVISIBLE);
        }}
        catch (Exception e)
        {
            thirdCard.setVisibility(View.INVISIBLE);
        }

        try {
            if (array[3].trim().length() == 0) {
                fourthCard.setVisibility(View.INVISIBLE);
            }
        }
        catch (Exception e)
        {
            fourthCard.setVisibility(View.INVISIBLE);
        }
        try {
            if (array[4].trim().length() == 0) {
                fifthCard.setVisibility(View.INVISIBLE);
            }
        }catch (Exception e)
        {
            fifthCard.setVisibility(View.INVISIBLE);
        }

        try{
        if (array[5].trim().length()==0)
        {
            sixthCard.setVisibility(View.INVISIBLE);
        }}
        catch (Exception e)
        {
            sixthCard.setVisibility(View.INVISIBLE);
        }




        return v;
    }

    public static InstagramPhotoFragment newInstance(String[] background, int position) {

        InstagramPhotoFragment f = new InstagramPhotoFragment();
        Bundle b = new Bundle();

        b.putStringArray("image", background);
        b.putInt("position",position);
        f.setArguments(b);

        return f;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser)
        {

        }
        else{

        }
    }

}
