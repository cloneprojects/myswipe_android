package com.app.myswipe.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.myswipe.R;


public class TitlePagerFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_title_pager, container, false);
try {
    TextView pageText = v.findViewById(R.id.pageText);
    pageText.setText(getArguments().getString("title"));
}catch (Exception e)
{

}

        return v;
    }

    public static TitlePagerFragment newInstance(String title, String background, int position) {

        TitlePagerFragment f = new TitlePagerFragment();
        Bundle b = new Bundle();
        b.putString("title", title);
        b.putString("background", background);
        b.putInt("position",position);
        f.setArguments(b);

        return f;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser)
        {

        }
        else{

        }
    }

}
