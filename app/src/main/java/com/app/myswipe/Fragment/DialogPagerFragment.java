package com.app.myswipe.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class DialogPagerFragment extends Fragment {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("DialogPagerFragment", "onCreate: Open");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // View v = inflater.inflate(R.layout.dialog_view_pager, null);


//        TextView text1 = v.findViewById(R.id.text1);
//        TextView text2 = v.findViewById(R.id.text2);
//        ImageView icons = v.findViewById(R.id.icons);
//
//        icons.setBackgroundResource(getArguments().getInt("icons"));
//        text1.setText(getArguments().getString("text1"));
//        text2.setText(getArguments().getString("text2"));

        return null;
    }

    public static DialogPagerFragment newInstance(int icon, String text1, String text2) {

        DialogPagerFragment f = new DialogPagerFragment();
        Bundle b = new Bundle();
        b.putString("title", text1);
        b.putString("subtitle", text2);
        b.putInt("icons", icon);
        f.setArguments(b);

        return f;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

        } else {

        }
    }

}
