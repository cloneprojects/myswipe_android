package com.app.myswipe.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.app.myswipe.Adapters.SlidingImage_Adapter;
import com.app.myswipe.Adapters.goldSlidingImage_Adapter;
import com.app.myswipe.GoogleBilling.BillingManager;
import com.app.myswipe.R;
import com.app.myswipe.Stripe.ExampleEphemeralKeyProvider;
import com.app.myswipe.Volley.ApiCall;
import com.app.myswipe.Volley.LocationCallBack;
import com.app.myswipe.Volley.VolleyCallback;
import com.rd.PageIndicatorView;
import com.stripe.android.CustomerSession;
import com.stripe.android.model.Customer;
import com.stripe.android.model.CustomerSource;
import com.stripe.android.view.PaymentMethodsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;

/*
 * Created by user on 17-11-2017.
 */

public class Utils {

    //private PaymentsClient mPaymentsClient;
    public static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 507;

    private BillingManager billingManager;
    private final String skuIdFirst = "super_swipe_1";
    private final String skuIdSecond = "super_swipe_2";

    private final String skuIdFirstB = "super_swipe_3";
    private final String skuIdSecondB = "super_swipe_4";

    private AppSettings appSettings;
    private Activity utilActivity;
    private String packageType;
    private String amount;
    private String amountType;


    public static final int REQUEST_CODE_SELECT_SOURCE = 55;
    private static Boolean isShowing = false;
    public static Boolean isInternetShowing = false;
    private static CustomDialog customDialog;
    private static CustomDialog IntercustomDialog;
    private static String TAG = Utils.class.getSimpleName();
    private TextView dismissButton;
    private ImageView gradientBg;
    private String[] titles = new String[6];
    private String[] subtitles = new String[6];
    private CardView cardBackgroundOne, cardBackgroundTwo, cardBackgroundThree;
    private FrameLayout firstImage, secondImage, thirdImage;
    private LinearLayout topOne, topTwo, topThree;
    private LinearLayout parentOne, parentTwo, parentThree;
    private ViewPager ViewPager;
    private PageIndicatorView circleIndicator;
    private int[] icons = new int[]{R.drawable.dialog_location, R.drawable.fire_icon, R.drawable.dialog_key, R.drawable.dialog_switch, R.drawable.heart_icon, R.drawable.forward_icon, R.drawable.dialog_stop};
    private int[] goldicons = new int[]{R.drawable.dialog_location, R.drawable.fire_icon, R.drawable.dialog_key, R.drawable.dialog_switch, R.drawable.heart_gold, R.drawable.forward_icon, R.drawable.dialog_stop};

    private int currentitem = 0;
    private Button continueButton;
    private Dialog dialog;


    public Utils(Activity activity) {
        appSettings = new AppSettings(activity);
        utilActivity = activity;
    }


    public static void toast(Context context, String toastmsg) {

        Toast.makeText(context, toastmsg, Toast.LENGTH_SHORT).show();
    }

    public static void getLocationAddres(final Activity context, final String currentLatitude, final String currentLongitude, final LocationCallBack locationCallBack) {
        String url = "https://maps.googleapis.com/maps/api/geocode/json?key=" + context.getResources().getString(R.string.map_key) + "&latlng=" + currentLatitude + "," + currentLongitude + "&sensor=true";
        Utils.log(TAG, url);
        ApiCall.getMethod(context, url, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String Address1 = "";
                    String Address2 = "";
                    String City = "";
                    String State = "";
                    String Country = "";
                    String County = "";
                    String PIN = "";
                    String Area = "";

                    JSONArray Results = response.getJSONArray("results");
                    JSONObject zero = Results.getJSONObject(0);
                    JSONArray address_components = zero.getJSONArray("address_components");


                    for (int i = 0; i < address_components.length(); i++) {
                        JSONObject zero2 = address_components.getJSONObject(i);
                        String long_name = zero2.getString("long_name");
                        JSONArray mtypes = zero2.getJSONArray("types");
                        String Type = mtypes.getString(0);

                        if (TextUtils.isEmpty(long_name) == false || !long_name.equals(null) || long_name.length() > 0 || long_name != "") {
                            if (Type.equalsIgnoreCase("street_number")) {
                                Address1 = long_name + " ";
                            } else if (Type.equalsIgnoreCase("route")) {
                                Address1 = Address1 + long_name;
                            } else if (Type.equalsIgnoreCase("sublocality")) {
                                Address2 = long_name;
                            } else if (Type.equalsIgnoreCase("locality")) {
                                // Address2 = Address2 + long_name + ", ";
                                City = long_name;
                            } else if (Type.equalsIgnoreCase("administrative_area_level_2")) {
                                County = long_name;
                            } else if (Type.equalsIgnoreCase("administrative_area_level_1")) {
                                State = long_name;
                            } else if (Type.equalsIgnoreCase("country")) {
                                Country = long_name;
                            } else if (Type.equalsIgnoreCase("political")) {
                                Area = long_name;
                            } else if (Type.equalsIgnoreCase("postal_code")) {
                                PIN = long_name;
                            }
                        }
                    }

                    locationCallBack.onLocationSuccess(City, State, Country);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromUriNew(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static String getPath(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        Uri uri = Uri.parse(path);
        Cursor cursor = inContext.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static void log(String TAG, String content) {
        Log.d(TAG, " " + content);
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void show(Activity context) {
        if (!context.isFinishing()) {
            if (!isShowing) {
                isShowing = true;
                customDialog = new CustomDialog(context);
                customDialog.setCancelable(false);
                customDialog.setCanceledOnTouchOutside(false);
                customDialog.setContentView(R.layout.custom_dialog);
                customDialog.show();
            }
        }


    }

    public static void showNoInternet(final Context context) {
        android.os.Handler handler = new android.os.Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.setContentView(R.layout.no_internet_layout);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                dialog.show();
                TextView confirmButton = (TextView) dialog.findViewById(R.id.confirmButton);
                confirmButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

    }

    public static void dismiss(Activity activity) {
        if (!activity.isFinishing()) {
            try {
                if (isShowing) {
                    isShowing = false;
                    customDialog.dismiss();
                }
            } catch (Exception e) {
                if (isShowing) {
                    isShowing = false;

                } else {
                    isShowing = true;

                }
            }
        } else {
            isShowing = !isShowing;
        }
    }

    public static String getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public void dismissGoldLayout() {
        try {
            dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showgoldLayout(final Activity context) {
        // initCustomerSession(context);
        initGooglePay(context);
        packageType = "gold";
        titles = new String[]{context.getResources().getString(R.string.swipe_around_the), context.getResources().getString(R.string.get_free), context.getResources().getString(R.string.choose_who_sees_you), context.getResources().getString(R.string.control_your_profile), context.getResources().getString(R.string.unlimited_likes), context.getResources().getString(R.string.unlimited_rewinds), context.getResources().getString(R.string.turn_off)};
        subtitles = new String[]{context.getResources().getString(R.string.passport_to), context.getResources().getString(R.string.skip_the_line), context.getResources().getString(R.string.only_be_shown), context.getResources().getString(R.string.limit_what_others), context.getResources().getString(R.string.swipe_right_as), context.getResources().getString(R.string.go_back_and), context.getResources().getString(R.string.have_fun)};

        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.gold_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        AppSettings appSettings = new AppSettings(context);
        appSettings.setSelectedRole("gold");
        //appSettings.setUserType("gold");


        ViewPager = (ViewPager) dialog.findViewById(R.id.ViewPager);
        gradientBg = (ImageView) dialog.findViewById(R.id.gradientBg);

        circleIndicator = (PageIndicatorView) dialog.findViewById(R.id.circleIndicator);


        cardBackgroundOne = (CardView) dialog.findViewById(R.id.cardBackgroundOne);
        cardBackgroundTwo = (CardView) dialog.findViewById(R.id.cardBackgroundTwo);
        cardBackgroundThree = (CardView) dialog.findViewById(R.id.cardBackgroundThree);

        firstImage = (FrameLayout) dialog.findViewById(R.id.firstImage);
        secondImage = (FrameLayout) dialog.findViewById(R.id.secondImage);
        thirdImage = (FrameLayout) dialog.findViewById(R.id.thirdImage);

        topOne = (LinearLayout) dialog.findViewById(R.id.topOne);
        topTwo = (LinearLayout) dialog.findViewById(R.id.topTwo);
        topThree = (LinearLayout) dialog.findViewById(R.id.topThree);

        parentOne = (LinearLayout) dialog.findViewById(R.id.parentOne);
        parentTwo = (LinearLayout) dialog.findViewById(R.id.parentTwo);
        parentThree = (LinearLayout) dialog.findViewById(R.id.parentThree);

        ColorDrawable colorDrawable = new ColorDrawable(context.getResources().getColor(R.color.transparent));
        gradientBg.setBackgroundDrawable(colorDrawable);
        dismissButton = (TextView) dialog.findViewById(R.id.dismissButton);
        continueButton = (Button) dialog.findViewById(R.id.continueButton);

        setGoldfirstSelected(context);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   launchWithCustomer(context);

                if (amountType.equalsIgnoreCase("1")) {
                    startGooglePay(skuIdFirstB);

                } else if (amountType.equalsIgnoreCase("2")) {
                    startGooglePay(skuIdSecondB);

                } else if (amountType.equalsIgnoreCase("3")) {
                    startGooglePay(skuIdFirstB);
                }

            }
        });

        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        cardBackgroundOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGoldfirstSelected(context);
            }
        });

        cardBackgroundTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGoldSecondSelected(context);
            }
        });

        cardBackgroundThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGoldThirdSelected(context);
            }
        });


        topOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGoldfirstSelected(context);
            }
        });

        topTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGoldSecondSelected(context);
            }
        });

        topThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGoldThirdSelected(context);
            }
        });

        ViewPager.setAdapter(new goldSlidingImage_Adapter(titles, subtitles, goldicons, context));

//        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
//        ViewPager.setAdapter(adapter);
        circleIndicator.setViewPager(ViewPager);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (currentitem < 8) {
                    currentitem++;
                } else {
                    currentitem = 0;
                }
                ViewPager.setCurrentItem(currentitem);
                handler.postDelayed(this, 3000);

            }
        }, 3000);


        ViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentitem = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void showLayout(final Activity context) {
        initCustomerSession(context);
        titles = new String[]{context.getResources().getString(R.string.swipe_around_the), context.getResources().getString(R.string.get_free), context.getResources().getString(R.string.choose_who_sees_you), context.getResources().getString(R.string.control_your_profile), context.getResources().getString(R.string.unlimited_likes), context.getResources().getString(R.string.unlimited_rewinds), context.getResources().getString(R.string.turn_off)};
        subtitles = new String[]{context.getResources().getString(R.string.passport_to), context.getResources().getString(R.string.skip_the_line), context.getResources().getString(R.string.only_be_shown), context.getResources().getString(R.string.limit_what_others), context.getResources().getString(R.string.swipe_right_as), context.getResources().getString(R.string.go_back_and), context.getResources().getString(R.string.have_fun)};
        AppSettings appSettings = new AppSettings(context);
        appSettings.setSelectedRole("plus");
        // appSettings.setUserType("plus");
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();


        ViewPager = (ViewPager) dialog.findViewById(R.id.ViewPager);
        gradientBg = (ImageView) dialog.findViewById(R.id.gradientBg);

        circleIndicator = (PageIndicatorView) dialog.findViewById(R.id.circleIndicator);


        cardBackgroundOne = (CardView) dialog.findViewById(R.id.cardBackgroundOne);
        cardBackgroundTwo = (CardView) dialog.findViewById(R.id.cardBackgroundTwo);
        cardBackgroundThree = (CardView) dialog.findViewById(R.id.cardBackgroundThree);

        firstImage = (FrameLayout) dialog.findViewById(R.id.firstImage);
        secondImage = (FrameLayout) dialog.findViewById(R.id.secondImage);


        topOne = (LinearLayout) dialog.findViewById(R.id.topOne);
        topTwo = (LinearLayout) dialog.findViewById(R.id.topTwo);
        topThree = (LinearLayout) dialog.findViewById(R.id.topThree);

        parentOne = (LinearLayout) dialog.findViewById(R.id.parentOne);
        parentTwo = (LinearLayout) dialog.findViewById(R.id.parentTwo);
        parentThree = (LinearLayout) dialog.findViewById(R.id.parentThree);
        ColorDrawable colorDrawable = new ColorDrawable(context.getResources().getColor(R.color.transparent));
        gradientBg.setBackgroundDrawable(colorDrawable);

        dismissButton = (TextView) dialog.findViewById(R.id.dismissButton);
        continueButton = (Button) dialog.findViewById(R.id.continueButton);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                launchWithCustomer(context);

            }
        });

        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        setfirstSelected(context);

        gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.darkbluegradient));

        cardBackgroundOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setfirstSelected(context);
            }
        });

        cardBackgroundTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSecondSelected(context);
            }
        });

        cardBackgroundThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setThirdSelected(context);
            }
        });


        topOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setfirstSelected(context);
            }
        });

        topTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSecondSelected(context);
            }
        });

        topThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setThirdSelected(context);
            }
        });

        ViewPager.setAdapter(new SlidingImage_Adapter(titles, subtitles, icons, context));

//        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
//        ViewPager.setAdapter(adapter);
        circleIndicator.setViewPager(ViewPager);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (currentitem < 8) {
                    currentitem++;
                } else {
                    currentitem = 0;
                }
                ViewPager.setCurrentItem(currentitem);
                handler.postDelayed(this, 3000);

            }
        }, 3000);

        ViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                currentitem = position;

                if (position == 6) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.blackgradient));
                }
                if (position == 5) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.purplegradient));
                }
                if (position == 4) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.gradient));
                }
                if (position == 3) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.yellogradient));
                }
                if (position == 2) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.bluegradient));
                }
                if (position == 1) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.rosegradient));
                }
                if (position == 0) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.darkbluegradient));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    public void showGooglePay(final Activity context) {
        // billingUtils = new BillingUtils(context);

        initGooglePay(context);
        packageType = "plus";

        titles = new String[]{context.getResources().getString(R.string.swipe_around_the), context.getResources().getString(R.string.get_free), context.getResources().getString(R.string.choose_who_sees_you), context.getResources().getString(R.string.control_your_profile), context.getResources().getString(R.string.unlimited_likes), context.getResources().getString(R.string.unlimited_rewinds), context.getResources().getString(R.string.turn_off)};
        subtitles = new String[]{context.getResources().getString(R.string.passport_to), context.getResources().getString(R.string.skip_the_line), context.getResources().getString(R.string.only_be_shown), context.getResources().getString(R.string.limit_what_others), context.getResources().getString(R.string.swipe_right_as), context.getResources().getString(R.string.go_back_and), context.getResources().getString(R.string.have_fun)};
        AppSettings appSettings = new AppSettings(context);
        appSettings.setSelectedRole("plus");
        //appSettings.setUserType("plus");
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();


        ViewPager = (ViewPager) dialog.findViewById(R.id.ViewPager);
        gradientBg = (ImageView) dialog.findViewById(R.id.gradientBg);

        circleIndicator = (PageIndicatorView) dialog.findViewById(R.id.circleIndicator);


        cardBackgroundOne = (CardView) dialog.findViewById(R.id.cardBackgroundOne);
        cardBackgroundTwo = (CardView) dialog.findViewById(R.id.cardBackgroundTwo);
        cardBackgroundThree = (CardView) dialog.findViewById(R.id.cardBackgroundThree);

        firstImage = (FrameLayout) dialog.findViewById(R.id.firstImage);
        secondImage = (FrameLayout) dialog.findViewById(R.id.secondImage);
        thirdImage = (FrameLayout) dialog.findViewById(R.id.thirdImage);


        topOne = (LinearLayout) dialog.findViewById(R.id.topOne);
        topTwo = (LinearLayout) dialog.findViewById(R.id.topTwo);
        topThree = (LinearLayout) dialog.findViewById(R.id.topThree);

        parentOne = (LinearLayout) dialog.findViewById(R.id.parentOne);
        parentTwo = (LinearLayout) dialog.findViewById(R.id.parentTwo);
        parentThree = (LinearLayout) dialog.findViewById(R.id.parentThree);
        ColorDrawable colorDrawable = new ColorDrawable(context.getResources().getColor(R.color.transparent));
        gradientBg.setBackgroundDrawable(colorDrawable);

        dismissButton = (TextView) dialog.findViewById(R.id.dismissButton);
        continueButton = (Button) dialog.findViewById(R.id.continueButton);

        setfirstSelected(context);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (amountType.equalsIgnoreCase("1")) {
                    startGooglePay(skuIdFirst);

                } else if (amountType.equalsIgnoreCase("2")) {
                    startGooglePay(skuIdSecond);

                } else if (amountType.equalsIgnoreCase("3")) {
                    startGooglePay(skuIdFirst);
                }
            }
        });

        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.darkbluegradient));

        cardBackgroundOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setfirstSelected(context);
            }
        });

        cardBackgroundTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setSecondSelected(context);
            }
        });

        cardBackgroundThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setThirdSelected(context);
            }
        });


        topOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setfirstSelected(context);
            }
        });

        topTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setSecondSelected(context);
            }
        });

        topThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setThirdSelected(context);
            }
        });

        ViewPager.setAdapter(new SlidingImage_Adapter(titles, subtitles, icons, context));

//        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
//        ViewPager.setAdapter(adapter);
        circleIndicator.setViewPager(ViewPager);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (currentitem < 8) {
                    currentitem++;
                } else {
                    currentitem = 0;
                }
                ViewPager.setCurrentItem(currentitem);
                handler.postDelayed(this, 3000);

            }
        }, 3000);

        ViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                currentitem = position;

                if (position == 6) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.blackgradient));
                }
                if (position == 5) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.purplegradient));
                }
                if (position == 4) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.gradient));
                }
                if (position == 3) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.yellogradient));
                }
                if (position == 2) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.bluegradient));
                }
                if (position == 1) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.rosegradient));
                }
                if (position == 0) {
                    gradientBg.setImageDrawable(context.getResources().getDrawable(R.drawable.darkbluegradient));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initGooglePay(Activity activity) {
        billingManager = new BillingManager(activity, new MyBillingUpdateListener());
    }

    private void startGooglePay(String skuId) {
        billingManager.initiatePurchaseFlow(skuId, null);
    }

    //google billing
    public class MyBillingUpdateListener implements BillingManager.BillingUpdatesListener {

        @Override
        public void onBillingClientSetupFinished() {
            Log.e(TAG, "onBillingClientSetupFinished: ");
            billingManager.queryPurchases();

        }

        @Override
        public void onConsumeFinished(String token, int result) {
            Log.e(TAG, "onConsumeFinished: " + token + " Result: " + result);
            if (result == BillingClient.BillingResponse.OK) {

            }
        }


        @Override
        public void onPurchasesUpdated(List<Purchase> purchases, int responseCode) {
            Log.e(TAG, "onPurchasesUpdated: " + purchases + " Code: " + responseCode);
            long current = System.currentTimeMillis() - 10000;

            String status;
            if (responseCode == BillingClient.BillingResponse.OK) {
                if (purchases != null) {
                    if (purchases.size() != 0) {
                        for (Purchase p : purchases) {
                            Log.e(TAG, "currentTime: " + current);
                            Log.e(TAG, "purchaseTime: " + p.getPurchaseTime());

                            if (current <= p.getPurchaseTime()) {
                                Log.e(TAG, "current: ");
                                status = "true";
                                dismissGooglePay(packageType, amount, status);
                            }
                            //update ui
                        }
                    }
                }

            } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
                dismissGoldLayout();

            } else if (responseCode == BillingClient.BillingResponse.ITEM_ALREADY_OWNED) {
                dismissGoldLayout();

            } else {
                Log.e(TAG, "onPurchasesUpdated: else part");
                status = "false";
                dismissGooglePay(packageType, amount, status);
            }
        }
    }


    private void dismissGooglePay(String packageType, String amount, String status) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("packageType", packageType);
            jsonObject.put("amount", amount);
            jsonObject.put("status", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiCall.PostMethodHeaders(utilActivity, UrlHelper.GOOGLE_PAY, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dismissGoldLayout();
            }
        });
    }


    public void initCustomerSession(Activity activity) {
        CustomerSession.initCustomerSession(
                new ExampleEphemeralKeyProvider(
                        new ExampleEphemeralKeyProvider.ProgressListener() {
                            @Override
                            public void onStringResponse(String string) {
                                if (string.startsWith("Error: ")) {

                                }
                            }
                        }, activity));

        CustomerSession.getInstance().retrieveCurrentCustomer(
                new CustomerSession.CustomerRetrievalListener() {
                    @Override
                    public void onCustomerRetrieved(@NonNull Customer customer) {
                        try {
                            CustomerSource sourcee = customer.getSourceById(customer.getDefaultSource());
                            String selectedSource = sourcee.toString();
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject = new JSONObject(selectedSource);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(int errorCode, @Nullable String errorMessage) {
                        Log.d("chek", "error: ");


                    }
                });

    }

    private void launchWithCustomer(Activity activity) {
        Intent payIntent = PaymentMethodsActivity.newIntent(activity);
        activity.startActivityForResult(payIntent, REQUEST_CODE_SELECT_SOURCE);
    }


    private void setfirstSelected(Context context) {
        amountType = "1";
        amount = "5";

        cardBackgroundOne.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        cardBackgroundTwo.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));
        cardBackgroundThree.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));

        cardBackgroundOne.setCardElevation(10);
        cardBackgroundTwo.setCardElevation(0);
        cardBackgroundThree.setCardElevation(0);


        firstImage.setVisibility(View.VISIBLE);
        secondImage.setVisibility(View.INVISIBLE);
        thirdImage.setVisibility(View.INVISIBLE);

        parentOne.setBackgroundColor(context.getResources().getColor(R.color.white));
        parentTwo.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));
        parentThree.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));

    }

    private void setSecondSelected(Context context) {
        amountType = "2";
        amount = "10";

        cardBackgroundOne.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));
        cardBackgroundTwo.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        cardBackgroundThree.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));

        cardBackgroundOne.setCardElevation(0);
        cardBackgroundTwo.setCardElevation(10);
        cardBackgroundThree.setCardElevation(0);


        firstImage.setVisibility(View.INVISIBLE);
        secondImage.setVisibility(View.VISIBLE);
        thirdImage.setVisibility(View.INVISIBLE);

        parentOne.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));
        parentTwo.setBackgroundColor(context.getResources().getColor(R.color.white));
        parentThree.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));

    }

    private void setThirdSelected(Context context) {
        amountType = "3";
        amount = "5";

        cardBackgroundOne.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));
        cardBackgroundTwo.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));
        cardBackgroundThree.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

        cardBackgroundOne.setCardElevation(0);
        cardBackgroundTwo.setCardElevation(0);
        cardBackgroundThree.setCardElevation(10);


        firstImage.setVisibility(View.INVISIBLE);
        secondImage.setVisibility(View.INVISIBLE);
        thirdImage.setVisibility(View.VISIBLE);

        parentOne.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));
        parentTwo.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));
        parentThree.setBackgroundColor(context.getResources().getColor(R.color.white));

    }

    private void setGoldfirstSelected(Context context) {

        amountType = "1";
        amount = "5";

        cardBackgroundOne.setCardBackgroundColor(context.getResources().getColor(R.color.goldstart));
        cardBackgroundTwo.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));
        cardBackgroundThree.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));

        cardBackgroundOne.setCardElevation(10);
        cardBackgroundTwo.setCardElevation(0);
        cardBackgroundThree.setCardElevation(0);


        firstImage.setVisibility(View.VISIBLE);
        secondImage.setVisibility(View.INVISIBLE);
        thirdImage.setVisibility(View.INVISIBLE);

        parentOne.setBackgroundColor(context.getResources().getColor(R.color.white));
        parentTwo.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));
        parentThree.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));

    }

    private void setGoldSecondSelected(Context context) {

        amountType = "2";
        amount = "10";

        cardBackgroundOne.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));
        cardBackgroundTwo.setCardBackgroundColor(context.getResources().getColor(R.color.goldstart));
        cardBackgroundThree.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));

        cardBackgroundOne.setCardElevation(0);
        cardBackgroundTwo.setCardElevation(10);
        cardBackgroundThree.setCardElevation(0);


        firstImage.setVisibility(View.INVISIBLE);
        secondImage.setVisibility(View.VISIBLE);
        thirdImage.setVisibility(View.INVISIBLE);

        parentOne.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));
        parentTwo.setBackgroundColor(context.getResources().getColor(R.color.white));
        parentThree.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));

    }

    private void setGoldThirdSelected(Context context) {

        amountType = "3";
        amount = "5";

        cardBackgroundOne.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));
        cardBackgroundTwo.setCardBackgroundColor(context.getResources().getColor(R.color.transparent));
        cardBackgroundThree.setCardBackgroundColor(context.getResources().getColor(R.color.goldstart));

        cardBackgroundOne.setCardElevation(0);
        cardBackgroundTwo.setCardElevation(0);
        cardBackgroundThree.setCardElevation(10);


        firstImage.setVisibility(View.INVISIBLE);
        secondImage.setVisibility(View.INVISIBLE);
        thirdImage.setVisibility(View.VISIBLE);

        parentOne.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));
        parentTwo.setBackgroundColor(context.getResources().getColor(R.color.settings_bg));
        parentThree.setBackgroundColor(context.getResources().getColor(R.color.white));

    }

    private void hideSoftKeyBoard(Activity context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
        }
    }


}
