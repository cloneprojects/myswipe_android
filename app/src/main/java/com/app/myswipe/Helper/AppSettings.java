package com.app.myswipe.Helper;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 23-11-2017.
 */

public class AppSettings {
    private String accessToken;
    private Context context;
    private String userImageUrl;
    private String userId;
    private String imagePath;
    private String isLogged;
    private String aboutUs;
    String selectedRole;
    String jobTitle;
    String company;
    String college;
    String gender;
    String age;
    String distance;
    String userName;
    String loginType;
    String isInstagramConnected;
    String instagramName;
    private String instagramToken;
    String instagramId;
    String userType;
    String totalCount;
    JSONArray instaGramPhotos;
    String imageValue;
    String fbDetails;
    String fireBaseToken;
    String mobileNumber;
    String showAge;
    String showDistance;
    JSONArray imagesArray;
    String likesRemaing;

    public String getLikesRemaing() {
        likesRemaing = SharedPreference.getKey(context, "likesRemaing");

        return likesRemaing;
    }

    public void setLikesRemaing(String likesRemaing) {
        SharedPreference.putKey(context, "likesRemaing", likesRemaing);

        this.likesRemaing = likesRemaing;
    }

    public String getSuperlikesRemaing() {
        superlikesRemaing = SharedPreference.getKey(context, "superlikesRemaing");

        return superlikesRemaing;
    }

    public void setSuperlikesRemaing(String superlikesRemaing) {
        SharedPreference.putKey(context, "superlikesRemaing", superlikesRemaing);

        this.superlikesRemaing = superlikesRemaing;
    }

    String superlikesRemaing;
    private String TAG = AppSettings.class.getSimpleName();

    public AppSettings(Context context) {
        this.context = context;
    }

    public String getUserType() {
        userType = SharedPreference.getKey(context, "userType");

        return userType;
    }

    public void setUserType(String userType) {
        SharedPreference.putKey(context, "userType", userType);

        this.userType = userType;
    }

    public String getSelectedRole() {
        selectedRole = SharedPreference.getKey(context, "selectedRole");

        return selectedRole;
    }

    public void setSelectedRole(String selectedRole) {
        SharedPreference.putKey(context, "selectedRole", selectedRole);

        this.selectedRole = selectedRole;
    }


    public String getInstagramToken() {
        instagramToken = SharedPreference.getKey(context, "instagramToken");

        return instagramToken;
    }

    public void setInstagramToken(String instagramToken) {
        this.instagramToken = instagramToken;
        SharedPreference.putKey(context, "instagramToken", instagramToken);

    }

    public String getInstagramId() {
        instagramId = SharedPreference.getKey(context, "instagramId");

        return instagramId;
    }

    public void setInstagramId(String instagramId) {
        SharedPreference.putKey(context, "instagramId", instagramId);

        this.instagramId = instagramId;
    }

    public String getInstagramName() {
        instagramName = SharedPreference.getKey(context, "instagramName");

        return instagramName;
    }

    public void setInstagramName(String instagramName) {

        SharedPreference.putKey(context, "instagramName", instagramName);

        this.instagramName = instagramName;
    }


    public void reduceSwipeCount(String type) {
        if (type.equalsIgnoreCase("like")) {
            int val = Integer.parseInt(SharedPreference.getKey(context, "likesRemaing"));
            if (val != 0) {
                val = val - 1;
                Log.e("SwipeCount", "reduceLike: " + val);
                setLikesRemaing(String.valueOf(val));
            }

        } else {
            int val = Integer.parseInt(SharedPreference.getKey(context, "superlikesRemaing"));

            if (val != 0) {
                val = val - 1;
                Log.e("SwipeCount", "reduceSuperLike: " + val);
                setSuperlikesRemaing(String.valueOf(val));
            }
        }

    }


    public JSONArray getInstaGramPhotos() throws JSONException {
        instaGramPhotos = new JSONArray();
        int totalCount = Integer.parseInt(getTotalCount());
        if (totalCount > 0) {
            for (int i = 0; i < totalCount; i++) {
                JSONObject jsonObject = new JSONObject();
                String image = SharedPreference.getKey(context, "instaimage" + i);
                jsonObject.put("image", image);
                Log.d(TAG, "getInstaGramPhotos: " + image);
                instaGramPhotos.put(jsonObject);
            }

        }
        return instaGramPhotos;
    }

    public void setInstaGramPhotos(JSONArray instaGramPhotos) {

        for (int i = 0; i < instaGramPhotos.length(); i++) {
            JSONObject jsonObject = instaGramPhotos.optJSONObject(i);
            String image = jsonObject.optString("image");
            Log.d(TAG, "setInstaGramPhotos: " + image);
            SharedPreference.putKey(context, "instaimage" + i, image);
        }
        this.instaGramPhotos = instaGramPhotos;
    }

    public String getIsInstagramConnected() {
        isInstagramConnected = SharedPreference.getKey(context, "isInstagramConnected");

        return isInstagramConnected;
    }

    public void setIsInstagramConnected(String isInstagramConnected) {
        SharedPreference.putKey(context, "isInstagramConnected", isInstagramConnected);

        this.isInstagramConnected = isInstagramConnected;
    }

    public String getTotalCount() {
        totalCount = SharedPreference.getKey(context, "totalCount");

        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        SharedPreference.putKey(context, "totalCount", totalCount);
        this.totalCount = totalCount;
    }

    public String getImageValue() {
        imageValue = SharedPreference.getKey(context, "imageValue");

        return imageValue;
    }

    public void setImageValue(String imageValue) {
        SharedPreference.putKey(context, "imageValue", imageValue);

        this.imageValue = imageValue;
    }

    public String getLoginType() {
        loginType = SharedPreference.getKey(context, "loginType");

        return loginType;
    }

    public void setLoginType(String loginType) {
        SharedPreference.putKey(context, "loginType", loginType);

        this.loginType = loginType;
    }

    public String getFireBaseToken() {
        fireBaseToken = SharedPreference.getKey(context, "fireBaseToken");

        return fireBaseToken;
    }

    public void setFireBaseToken(String fireBaseToken) {
        SharedPreference.putKey(context, "fireBaseToken", fireBaseToken);

        this.fireBaseToken = fireBaseToken;
    }

    public String getMobileNumber() {
        mobileNumber = SharedPreference.getKey(context, "mobileNumber");

        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        SharedPreference.putKey(context, "mobileNumber", mobileNumber);

        this.mobileNumber = mobileNumber;
    }

    public String getFbDetails() {
        fbDetails = SharedPreference.getKey(context, "fbDetails");

        return fbDetails;
    }

    public void setFbDetails(String fbDetails) {
        SharedPreference.putKey(context, "fbDetails", fbDetails);

        this.fbDetails = fbDetails;
    }

    public String getAge() {
        age = SharedPreference.getKey(context, "age");

        return age;
    }

    public void setAge(String age) {
        SharedPreference.putKey(context, "age", age);

        this.age = age;
    }

    public String getDistance() {
        distance = SharedPreference.getKey(context, "distance");

        return distance;
    }

    public void setDistance(String distance) {
        SharedPreference.putKey(context, "distance", distance);

        this.distance = distance;
    }

    public String getUserName() {
        userName = SharedPreference.getKey(context, "userName");

        return userName;
    }

    public void setUserName(String userName) {
        SharedPreference.putKey(context, "userName", userName);

        this.userName = userName;
    }

    public String getAboutUs() {
        aboutUs = SharedPreference.getKey(context, "aboutUs");

        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        SharedPreference.putKey(context, "aboutUs", aboutUs);

        this.aboutUs = aboutUs;
    }

    public String getJobTitle() {
        jobTitle = SharedPreference.getKey(context, "jobTitle");

        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        SharedPreference.putKey(context, "jobTitle", jobTitle);

        this.jobTitle = jobTitle;
    }

    public String getCompany() {
        company = SharedPreference.getKey(context, "company");

        return company;
    }

    public void setCompany(String company) {
        SharedPreference.putKey(context, "company", company);

        this.company = company;
    }

    public String getCollege() {

        college = SharedPreference.getKey(context, "college");

        return college;
    }

    public void setCollege(String college) {
        SharedPreference.putKey(context, "college", college);

        this.college = college;
    }

    public String getGender() {
        gender = SharedPreference.getKey(context, "gender");

        return gender;
    }

    public void setGender(String gender) {
        SharedPreference.putKey(context, "gender", gender);

        this.gender = gender;
    }

    public String getShowAge() {
        showAge = SharedPreference.getKey(context, "showAge");

        return showAge;
    }

    public void setShowAge(String showAge) {
        SharedPreference.putKey(context, "showAge", showAge);

        this.showAge = showAge;
    }

    public String getShowDistance() {
        showDistance = SharedPreference.getKey(context, "showDistance");

        return showDistance;
    }

    public void setShowDistance(String showDistance) {
        SharedPreference.putKey(context, "showDistance", showDistance);

        this.showDistance = showDistance;
    }

    public JSONArray getImagesArray() {
        try {
            imagesArray = new JSONArray(SharedPreference.getKey(context, "imagesArray"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return imagesArray;
    }

    public void setImagesArray(JSONArray imagesArray) {
        SharedPreference.putKey(context, "imagesArray", imagesArray.toString());

        this.imagesArray = imagesArray;
    }

    public String getUserId() {
        userId = SharedPreference.getKey(context, "userId");

        return userId;
    }

    public void setUserId(String userId) {
        SharedPreference.putKey(context, "userId", userId);

        this.userId = userId;
    }

    public String getUserImageUrl() {
        userImageUrl = SharedPreference.getKey(context, "userImageUrl");

        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        SharedPreference.putKey(context, "userImageUrl", userImageUrl);

        this.userImageUrl = userImageUrl;
    }

    public String getImagePath() {
        imagePath = SharedPreference.getKey(context, "imagePath");

        return imagePath;
    }

    public void setImagePath(String imagePath) {
        SharedPreference.putKey(context, "imagePath", imagePath);

        this.imagePath = imagePath;
    }

    public String getIsLogged() {
        isLogged = SharedPreference.getKey(context, "isLogged");

        return isLogged;
    }

    public void setIsLogged(String isLogged) {
        SharedPreference.putKey(context, "isLogged", isLogged);

        this.isLogged = isLogged;
    }

    public String getAccessToken() {
        accessToken = SharedPreference.getKey(context, "accessToken");
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        SharedPreference.putKey(context, "accessToken", accessToken);
        this.accessToken = accessToken;
    }
}
