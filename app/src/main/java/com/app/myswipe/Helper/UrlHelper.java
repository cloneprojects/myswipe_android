package com.app.myswipe.Helper;

/**
 * Created by user on 23-11-2017.
 */

public class UrlHelper {

    //    public static String BASE="http://18.218.18.30:3001/admin/showPag";
    public static String BASE = "http://18.218.18.30:3001/";
    public static String BASE_URL = BASE + "user/";
    public static String REGISTER = BASE_URL + "register";
    public static String CALL_CODE = BASE_URL + "register1";
    public static String VERIFY = BASE_URL + "verify";
    public static String MAKE_PAYMENT = BASE_URL + "payment";
    public static String IMAGE_UPLOAD = BASE_URL + "profilePicUpload";
    public static String PROFILE_REGISTER = BASE_URL + "userData";
    //    public static String MAIN_SCREEN=BASE_URL+"mainScreen";
    public static String SEE_LIKED_LIST = BASE_URL + "seeLikedPerson";
    public static String MAIN_SCREEN = BASE_URL + "newmainScreen";
    public static String SWIPE = BASE_URL + "swipe";
    public static String VIEW_PROFILE = BASE_URL + "viewProfile";
    public static String EDIT_PROFILE = BASE_URL + "editProfile";
    public static String SETTINGS = BASE_URL + "settings";
    public static String DELETE_ACCCOUNT = BASE_URL + "deleteAccount";
    public static String GET_SETTINGS = BASE_URL + "settingRequest";
    public static String VIEW_MATCHES = BASE_URL + "Matches";
    public static String CHAT_LISTS = BASE_URL + "chatlist";
    public static String FB_CHECK = BASE_URL + "fbCheck";
    public static String UNMATCH = BASE_URL + "Unmatch";
    public static String DELETE_IMAGE = BASE_URL + "deleteImage";
    public static String INSERT_IMAGE = BASE_URL + "insertImage";
    public static String REPORT = BASE_URL + "report";
    public static String LOG_OUT = BASE_URL + "logout";
    public static String GOOGLE_PAY = BASE_URL + "iospayment";
    public static String ACTIVATE_INSTAGRAM = BASE_URL + "ActivateInstagram";
    public static String DEACTIVATE_INSTAGRAM = BASE_URL + "deActivateInstagram";
    public static String DEVICE_TOKEN = BASE_URL + "deviceToken";
    public static String PRIVACY_POLICY = "https://tinderboxsolutions.net/privacy-policy.html";
    public static String TERMS_SERVICES = BASE_URL + "Unmatch";
    public static String HELP_SUPPORT = "https://tinderboxsolutions.net/customer-support.html";
    public static String ADD_ADDRESS = BASE_URL + "locationChange";
}
