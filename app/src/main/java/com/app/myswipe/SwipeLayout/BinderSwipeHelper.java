package com.app.myswipe.SwipeLayout;


import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.app.myswipe.Fragment.HomeFragment;
import com.app.myswipe.Helper.AppSettings;


/**
 * Created by karthik on 24/08/16.
 */
public class BinderSwipeHelper implements View.OnTouchListener {

    private final BinderSwipeStack mSwipeStack;
    Context context;
    AppSettings appSettings;
    private View mObservedView;
    private View mLastVIew;
    private boolean mListenForTouchEvents;
    private float mDownX;
    private float mDownY;
    private float startX;
    private float startY;
    private float mInitialX;
    private float mInitialY;
    private int CLICK_ACTION_THRESHOLD = 200;
    private int mPointerId;
    private float mRotateDegrees = BinderSwipeStack.DEFAULT_SWIPE_ROTATION;
    private float mOpacityEnd = BinderSwipeStack.DEFAULT_SWIPE_OPACITY;
    private int mAnimationDuration = BinderSwipeStack.DEFAULT_ANIMATION_DURATION;

    public BinderSwipeHelper(BinderSwipeStack swipeStack, Context context) {

        this.context = context;
        mSwipeStack = swipeStack;

        appSettings = new AppSettings(context);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
//        Log.d("eventAction", "onTouch: " + event.getAction());
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                if (!mListenForTouchEvents || !mSwipeStack.isEnabled()) {
                    return false;
                }

                v.getParent().requestDisallowInterceptTouchEvent(true);

                mSwipeStack.onSwipeStart();

                mPointerId = event.getPointerId(0);
                try {
                    mDownX = event.getX(mPointerId);
                    mDownY = event.getY(mPointerId);
                } catch (Exception e) {

                }
                return true;

            case MotionEvent.ACTION_MOVE:
                int pointerIndex = event.findPointerIndex(mPointerId);
                if (pointerIndex < 0) return false;

                float dx = event.getX(pointerIndex) - mDownX;
                float dy = event.getY(pointerIndex) - mDownY;

                float newX = mObservedView.getX() + dx;
                float newY = mObservedView.getY() + dy;

                mObservedView.setX(newX);
                mObservedView.setY(newY);

                float dragDistanceX = newX - mInitialX;
                float dragDistanceY = newY - mInitialY;
                float swipeProgress = Math.min(Math.max(
                        dragDistanceX / mSwipeStack.getWidth(), -1), 1);

                mSwipeStack.onSwipeProgress(swipeProgress);


                if (dragDistanceX > 80) {
                    HomeFragment.update(0, dragDistanceX, dragDistanceY, mObservedView);
                } else if (dragDistanceX < -80)
                    HomeFragment.update(1, dragDistanceX, dragDistanceY, mObservedView);
                else if (dragDistanceY < -40)
                    HomeFragment.update(2, dragDistanceX, dragDistanceY, mObservedView);

                if (mRotateDegrees > 0) {
                    float rotation = mRotateDegrees * swipeProgress;
                    mObservedView.setRotation(rotation);
                }

                if (mOpacityEnd < 1f) {
                    float alpha = 1 - Math.min(Math.abs(swipeProgress * 2), 1);
                    mObservedView.setAlpha(alpha);
                }


                return true;

            case MotionEvent.ACTION_UP:
                try {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    float X = event.getX(mPointerId);
                    float Y = event.getY(mPointerId);
                    if (X == mDownX && Y == mDownY) {
//                        Log.d("clickValue", "onTouch: ClickHappened");

                        String positio;
//                        Log.d("clickValue", "onTouch: " + event.getX());
//                        Log.d("clickValue", "onTouchY: " + event.getY());
//                        Log.d("clickValue", "onTouchY: " + mObservedView.getHeight());

                        if (mObservedView.getHeight() - event.getY() < (mObservedView.getHeight() / 4)) {
                            positio = "click";

                        } else if (event.getX() < (mObservedView.getWidth() / 2)) {
                            positio = "left";
                        } else {
                            positio = "right";

                        }
                        mSwipeStack.onTopCardClicked(positio, mObservedView);
                    } else {
//                        Log.d("clickValue", "onTouch: NoHappened");

                    }


                    mSwipeStack.onSwipeEnd();
                    checkViewPosition();
                } catch (Exception e) {

                }


                return true;

        }

        return false;
    }


    private boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return !(differenceX > CLICK_ACTION_THRESHOLD/* =5 */ || differenceY > CLICK_ACTION_THRESHOLD);
    }

    private void checkViewPosition() {
        if (!mSwipeStack.isEnabled()) {
            resetViewPosition();
            return;
        }

        float viewCenterHorizontal = mObservedView.getX() + (mObservedView.getWidth() / 2);
        float viewCenterVertical = mObservedView.getY() + (mObservedView.getHeight() / 2);

        float parentFirstThird = mSwipeStack.getWidth() / 3f;
        float parentLastThird = parentFirstThird * 2;

        float parentFirstThirdHeight = mSwipeStack.getHeight() / 3f;
//        float parentLastThirdHeight = parentFirstThirdHeight * 2;

        if (viewCenterHorizontal < parentFirstThird &&
                mSwipeStack.getAllowedSwipeDirections() != BinderSwipeStack.SWIPE_DIRECTION_ONLY_RIGHT) {
            swipeViewToLeft(mAnimationDuration / 2);
        } else if (viewCenterHorizontal > parentLastThird &&
                mSwipeStack.getAllowedSwipeDirections() != BinderSwipeStack.SWIPE_DIRECTION_ONLY_LEFT) {

            if (appSettings.getUserType().equalsIgnoreCase("basic")) {
                if (Integer.parseInt(appSettings.getLikesRemaing()) != 0) {
                    swipeViewToRight(mAnimationDuration / 2);
                } else {
                    HomeFragment.showGoldFromSwipe((Activity) context);
                    resetViewPosition();
                }
            } else {
                swipeViewToRight(mAnimationDuration / 2);

            }
        } else if (viewCenterVertical < parentFirstThird) {
            if (Integer.parseInt(appSettings.getSuperlikesRemaing()) != 0) {
                swipeViewToTop(mAnimationDuration);
            } else {
                HomeFragment.showGoldFromSwipe((Activity) context);

                resetViewPosition();
            }
        } else {
            resetViewPosition();

        }
    }

    private void resetViewPosition() {
        HomeFragment.update(5, 0, 0, mObservedView);
        mObservedView.animate()
                .x(mInitialX)
                .y(mInitialY)
                .rotation(0)
                .alpha(1)
                .setDuration(mAnimationDuration)
                .setInterpolator(new OvershootInterpolator(1.4f))
                .setListener(null);
    }


    private void resetlastPosition() {
        if (mLastVIew != null) {
            HomeFragment.update(5, 0, 0, mLastVIew);

            mLastVIew.animate()
                    .x(mInitialX)
                    .y(mInitialY)
                    .rotation(0)
                    .alpha(1)
                    .setDuration(mAnimationDuration)
                    .setInterpolator(new OvershootInterpolator(1.4f))
                    .setListener(null);
        }
    }


    private void swipeViewToTop(int duration) {
        if (!mListenForTouchEvents) return;
        mListenForTouchEvents = false;
        mObservedView.animate().cancel();
        HomeFragment.setVisible(2, mObservedView);
        mLastVIew = mObservedView;

        mObservedView.animate()
                .y(-mSwipeStack.getHeight() + mObservedView.getY())
                .rotation(-mRotateDegrees)
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimUtils.AnimationEndListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mSwipeStack.onViewSwipedToTop(mObservedView);
                    }

                });


    }

    private void swipeViewToLeft(int duration) {
        if (!mListenForTouchEvents) return;
        mListenForTouchEvents = false;
        mObservedView.animate().cancel();
        HomeFragment.setVisible(1, mObservedView);
        mLastVIew = mObservedView;

        mObservedView.animate()
                .x(-mSwipeStack.getWidth() + mObservedView.getX())
                .rotation(-mRotateDegrees)
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimUtils.AnimationEndListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mSwipeStack.onViewSwipedToLeft(mObservedView);
                    }
                });

    }

    private void swipeViewToRight(int duration) {
        if (!mListenForTouchEvents) return;
        mListenForTouchEvents = false;
        mObservedView.animate().cancel();
        HomeFragment.setVisible(0, mObservedView);
        mLastVIew = mObservedView;


        mObservedView.animate()
                .x(mSwipeStack.getWidth() + mObservedView.getX())
                .rotation(mRotateDegrees)
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimUtils.AnimationEndListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mSwipeStack.onViewSwipedToRight(mObservedView);
                    }
                });
    }

    public void registerObservedView(View view, float initialX, float initialY) {
        if (view == null) {
            mObservedView = null;
            return;
        }
        if (mObservedView != null) {
            mObservedView.setOnTouchListener(null);
        }
        mObservedView = null;

        mListenForTouchEvents = false;

        mObservedView = view;
        mObservedView.setOnTouchListener(this);
        mInitialX = initialX;
        mInitialY = initialY;
        mListenForTouchEvents = true;
        HomeFragment.resetNext(mObservedView);
    }

    public void unregisterObservedView() {
        if (mObservedView != null) {
            mObservedView.setOnTouchListener(null);
        }
        mObservedView = null;

        mListenForTouchEvents = false;
    }

    public void setAnimationDuration(int duration) {
        mAnimationDuration = duration;
    }

    public void setRotation(float rotation) {
        mRotateDegrees = rotation;
    }

    public void setOpacityEnd(float alpha) {
        mOpacityEnd = alpha;
    }

    public void swipeViewToLeft() {
        swipeViewToLeft(mAnimationDuration);
    }

    public void swipeViewToRight() {
        swipeViewToRight(mAnimationDuration);
    }

    public void swipeViewToTop() {
        swipeViewToTop(mAnimationDuration);
    }

    public void resetLast() {
        resetlastPosition();
    }

}