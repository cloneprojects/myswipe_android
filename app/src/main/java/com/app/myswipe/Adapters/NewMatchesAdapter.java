package com.app.myswipe.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.myswipe.Activity.ChatActivity;
import com.app.myswipe.R;

import org.json.JSONArray;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by user on 22-11-2017.
 */

public class NewMatchesAdapter extends RecyclerView.Adapter<NewMatchesAdapter.MyViewHolder> implements Filterable {
    private JSONArray jsonArray;
    private JSONArray defArray;
    Context context;
    private CustomFilter customFilter;


    public NewMatchesAdapter(JSONArray jsonArray, Context context) {
        this.context = context;
        this.jsonArray = jsonArray;
        this.defArray = jsonArray;
        customFilter = new CustomFilter(NewMatchesAdapter.this, jsonArray);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_matches, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        JSONObject jsonObject = new JSONObject();
        jsonObject = jsonArray.optJSONObject(position);
        holder.userName.setText(jsonObject.optString("userName"));
        Glide.with(context).load(jsonObject.optString("displayPic"))
                .placeholder(R.drawable.pl_prof).into(holder.userImage);
        if (jsonObject.optString("isSuperLiked").equalsIgnoreCase("1")) {
            holder.isSuperLiked.setVisibility(View.VISIBLE);
        } else {
            holder.isSuperLiked.setVisibility(View.GONE);

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("type", "click");
                intent.putExtra("receiverId", jsonArray.optJSONObject(position).optString("senderID"));
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class CustomFilter extends Filter {
        private NewMatchesAdapter mAdapter;
        private JSONArray mJsonArray;

        private CustomFilter(NewMatchesAdapter mAdapter, JSONArray jsonArray) {
            super();
            this.mAdapter = mAdapter;
            this.mJsonArray = jsonArray;
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String charString = charSequence.toString();
            JSONArray jsonArrays = new JSONArray();
            Log.e("ccc", "charString: " + charSequence);
            if (charString.isEmpty()) {
                jsonArrays = defArray;
            } else {
                JSONArray filterArray = new JSONArray();
                for (int i = 0; i < mJsonArray.length(); i++) {
                    if (mJsonArray.optJSONObject(i).optString("userName").toLowerCase()
                            .contains(charString.toLowerCase())) {
                        Log.e("ccc", "String: ");
                        filterArray.put(mJsonArray.optJSONObject(i));
                    }
                }

                jsonArrays = filterArray;

            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = jsonArrays;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            jsonArray = (JSONArray) filterResults.values;
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public Filter getFilter() {
        return customFilter;
        /*return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                JSONArray jsonArrays = new JSONArray();
                Log.e("ccc", "charString: " + charSequence);
                if (charString.isEmpty()) {
                    jsonArrays = defArray;
                } else {
                    JSONArray filterArray = new JSONArray();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.optJSONObject(i).optString("userName").toLowerCase().contains(charString.toLowerCase())) {
                            Log.e("ccc", "String: ");
                            filterArray.put(jsonArray.optJSONObject(i));
                        }
                    }

                    jsonArrays = filterArray;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = jsonArrays;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                jsonArray = (JSONArray) filterResults.values;
                notifyDataSetChanged();

            }
        };*/
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView userName;
        ImageView isSuperLiked;

        public MyViewHolder(View itemView) {
            super(itemView);

            userImage = (CircleImageView) itemView.findViewById(R.id.userImage);
            isSuperLiked = (ImageView) itemView.findViewById(R.id.isSuperLiked);
            userName = (TextView) itemView.findViewById(R.id.userName);

        }


    }
}
