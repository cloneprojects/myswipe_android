package com.app.myswipe.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.myswipe.Activity.ChatActivity;
import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.R;

import org.json.JSONArray;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/*
 * Created by user on 22-11-2017.
 */

public class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomAdapter.MyViewHolder> implements Filterable {
    private JSONArray jsonArray;
    private JSONArray defArray = new JSONArray();
    Context context;
    private AppSettings appSettings;
    private CustomFilter customFilter;

    public ChatRoomAdapter(JSONArray jsonArray, Context context) {
        this.context = context;
        this.jsonArray = jsonArray;
        this.defArray = jsonArray;
        appSettings = new AppSettings(context);
        customFilter = new CustomFilter(ChatRoomAdapter.this, jsonArray);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.item_chat_rooms, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        JSONObject jsonObject = new JSONObject();
        jsonObject = jsonArray.optJSONObject(holder.getAdapterPosition());
        holder.userName.setText(jsonObject.optString("userName"));
        Glide.with(context).load(jsonObject.optString("displayPic"))
                .placeholder(R.drawable.pl_prof).into(holder.userImage);
        String type = jsonObject.optString("content_type");

        if (jsonObject.optString("isSuperLiked").equalsIgnoreCase("1")) {
            holder.isSuperLiked.setVisibility(View.VISIBLE);
        } else {
            holder.isSuperLiked.setVisibility(View.GONE);

        }

//
//        if (type.equalsIgnoreCase("text")) {
//            holder.lastMessage.setText(jsonObject.optString("lastMsg"));
//
//        } else {
//
//            holder.lastMessage.setText("sent a ");
//


//            if (type.equalsIgnoreCase("heart")) {
//                holder.lastMessageIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_heart));
//            } else if (type.equalsIgnoreCase("wow")) {
//                holder.lastMessageIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_surprised));
//            } else if (type.equalsIgnoreCase("angry")) {
//                holder.lastMessageIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_angry));
//            } else {
//                holder.lastMessageIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_laughing));
//            }

        holder.lastMessageIcon.setVisibility(View.GONE);

        if (type.equalsIgnoreCase("text")) {
            holder.lastMessage.setText(jsonObject.optString("lastMsg"));

        } else {
            if (jsonObject.optString("lastMsgSentBy").equalsIgnoreCase(appSettings.getUserId())) {
                String textt = context.getResources().getString(R.string.you_sent_a);
                if (type.equalsIgnoreCase("heart")) {
                    textt = textt + " ❤";

                } else if (type.equalsIgnoreCase("wow")) {
                    textt = textt + " \uD83D\uDE2E";


                } else if (type.equalsIgnoreCase("angry")) {
                    textt = textt + " \uD83D\uDE20";


                } else {
                    textt = textt + " \uD83D\uDE06";

                }
                holder.lastMessage.setText(textt);

            } else {

                String textt = jsonObject.optString("lastMsg");
                if (type.equalsIgnoreCase("heart")) {
                    textt = textt + " ❤";

                } else if (type.equalsIgnoreCase("wow")) {
                    textt = textt + " \uD83D\uDE2E";


                } else if (type.equalsIgnoreCase("angry")) {
                    textt = textt + " \uD83D\uDE20";


                } else {
                    textt = textt + " \uD83D\uDE06";

                }
                holder.lastMessage.setText(textt);

            }
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("values", jsonArray.optJSONObject(holder.getAdapterPosition()).toString());
                intent.putExtra("type", "click");
                intent.putExtra("receiverId", jsonArray.optJSONObject(holder.getAdapterPosition()).optString("senderID"));
                context.startActivity(intent);
            }
        });
    }

    public class CustomFilter extends Filter {
        private ChatRoomAdapter mAdapter;
        private JSONArray mJsonArray;

        private CustomFilter(ChatRoomAdapter mAdapter, JSONArray jsonArray) {
            super();
            this.mAdapter = mAdapter;
            this.mJsonArray = jsonArray;
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String charString = charSequence.toString();
            JSONArray jsonArrays = new JSONArray();
            Log.e("dddd", "charString: " + charSequence);
            if (charString.isEmpty()) {
                Log.e("dddd", "empty: ");
                jsonArrays = defArray;
            } else {

                Log.e("dddd", "not empty: ");
                JSONArray filterArray = new JSONArray();
                Log.e("dddd", "jsonArray: " + mJsonArray);
                for (int i = 0; i < mJsonArray.length(); i++) {
                    if (mJsonArray.optJSONObject(i).optString("userName")
                            .toLowerCase().contains(charString.toLowerCase())) {
                        Log.e("dddd", "matches: ");

                        filterArray.put(mJsonArray.optJSONObject(i));
                    }
                }

                jsonArrays = filterArray;

            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = jsonArrays;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {
            jsonArray = (JSONArray) filterResults.values;
            mAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public Filter getFilter() {
        return customFilter;
        /*return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                JSONArray jsonArrays = new JSONArray();
                Log.e("dddd", "charString: " + charSequence);
                if (charString.isEmpty()) {
                    Log.e("dddd", "empty: ");
                    jsonArrays = defArray;
                } else {

                    Log.e("dddd", "not empty: ");
                    JSONArray filterArray = new JSONArray();
                    Log.e("dddd", "jsonArray: " + jsonArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.optJSONObject(i).optString("userName")
                                .toLowerCase().contains(charString.toLowerCase())) {
                            Log.e("dddd", "matches: ");

                            filterArray.put(jsonArray.optJSONObject(i));
                        }
                    }

                    jsonArrays = filterArray;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = jsonArrays;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                jsonArray = (JSONArray) filterResults.values;
                notifyDataSetChanged();

            }
        };*/
    }

    @Override
    public int getItemCount() {
        Log.d("adapter", "getItemCount: " + jsonArray.length());
        return jsonArray.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView userName, lastMessage;
        ImageView lastMessageIcon, isSuperLiked;

        MyViewHolder(View itemView) {
            super(itemView);

            userImage = itemView.findViewById(R.id.userImage);
            lastMessageIcon = itemView.findViewById(R.id.lastMessageIcon);
            isSuperLiked = itemView.findViewById(R.id.isSuperLiked);
            userName = itemView.findViewById(R.id.userName);
            lastMessage = itemView.findViewById(R.id.lastMessage);

        }


    }
}
