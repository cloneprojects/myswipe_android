package com.app.myswipe.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.myswipe.Activity.ChatActivity;
import com.app.myswipe.Helper.AppSettings;
import com.app.myswipe.R;

import org.json.JSONArray;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/11/17.
 */

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.MyViewHolder> {
    JSONArray jsonArray;
    Context context;
    String user_name;

    public ChatsAdapter(JSONArray jsonArray, Context context, String user_name) {
        this.jsonArray = jsonArray;
        this.context = context;
        this.user_name = user_name;
    }

    public void addMessage(JSONObject jsonObject) {
        jsonArray.put(jsonObject);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sender_text_item, parent, false);

        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.receiver_text_item, parent, false);

        }
        return new MyViewHolder(view);
    }

    public static String convertDate(String dateInMilliseconds, String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }

    public void refresh()
    {
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        JSONObject jsonObject = jsonArray.optJSONObject(position);
        AppSettings appSettings = new AppSettings(context);
        holder.timeStamp.setText(convertDate(jsonObject.optString("Time"), "h:mm a"));
        String userType=jsonArray.optJSONObject(position).optString("type");

        if (userType.equalsIgnoreCase("sender")) {
            Glide.with(context).load(appSettings.getUserImageUrl()).into(holder.userImage);
        } else {
            Glide.with(context).load(ChatActivity.displayPic).into(holder.userImage);
        }

        if (jsonObject.optString("content_type").equalsIgnoreCase("text"))
        {
            holder.emojiLayout.setVisibility(View.GONE);
            holder.chatLayout.setVisibility(View.VISIBLE);
            holder.message.setText(jsonObject.optString("content"));
        } else {
            holder.emojiLayout.setVisibility(View.VISIBLE);
            holder.chatLayout.setVisibility(View.GONE);

            if (userType.equalsIgnoreCase("sender")) {
                holder.emojiText.setText(context.getResources().getString(R.string.you_sent_a)+" ");

            } else {
                holder.emojiText.setText(user_name+ " "+context.getResources().getString(R.string.sent_a));

            }


            if (jsonObject.optString("content_type").equalsIgnoreCase("heart"))
            {
                holder.emojiIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_heart));
            }  if (jsonObject.optString("content_type").equalsIgnoreCase("laugh"))
            {
                holder.emojiIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_laughing));
            }  if (jsonObject.optString("content_type").equalsIgnoreCase("wow"))
            {
                holder.emojiIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_surprised));
            }
            if (jsonObject.optString("content_type").equalsIgnoreCase("angry"))
            {
                holder.emojiIcon.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_angry));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        int type;

        if (jsonArray.optJSONObject(position).optString("type").equalsIgnoreCase("sender")) {
            type = 0;
        } else {
            type = 1;
        }
        return type;
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView message, timeStamp;
        CircleImageView userImage;
        LinearLayout emojiLayout;
        RelativeLayout chatLayout;
        ImageView emojiIcon;
        TextView emojiText;


        public MyViewHolder(View itemView) {
            super(itemView);
            userImage = (CircleImageView) itemView.findViewById(R.id.userImage);
            message = (TextView) itemView.findViewById(R.id.message);
            timeStamp = (TextView) itemView.findViewById(R.id.timeStamp);
            emojiText = (TextView) itemView.findViewById(R.id.emojiText);
            emojiIcon = (ImageView) itemView.findViewById(R.id.emojiIcon);
            emojiLayout = (LinearLayout) itemView.findViewById(R.id.emojiLayout);
            chatLayout = (RelativeLayout) itemView.findViewById(R.id.chatLayout);

        }
    }
}
