package com.app.myswipe.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.myswipe.Activity.SettingsActivity;
import com.app.myswipe.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 22-11-2017.
 */

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.MyViewHolder> {
    JSONArray jsonArray;

    Context context;


    public LocationListAdapter(JSONArray jsonArray, Context context) {
        this.context = context;
        this.jsonArray = jsonArray;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_list_items, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        JSONObject jsonObject = jsonArray.optJSONObject(position);


        holder.addressName.setVisibility(View.VISIBLE);
        holder.cityName.setText(jsonObject.optString("cityname"));
        holder.addressName.setText(jsonObject.optString("address"));



        if (jsonObject.optString("isSelected").equalsIgnoreCase("0")) {
            holder.isSelected.setVisibility(View.INVISIBLE);
            holder.markerLocation.setImageResource(R.drawable.unselected_location_marker);

        } else {
            holder.isSelected.setVisibility(View.VISIBLE);
            holder.markerLocation.setImageResource(R.drawable.selected_location_marker);

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    setSelectedValuesPosition(position);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public void refreshAdapter() throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.optJSONObject(i);
            jsonObject.remove("isSelected");
            jsonObject.put("isSelected", "0");
        }
        notifyDataSetChanged();

    }

    public void setSelectedValuesPosition(int position) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.optJSONObject(i);
            jsonObject.remove("isSelected");
            if (i == position) {
                jsonObject.put("isSelected", "1");
                SettingsActivity.selectedLocationId=jsonObject.optString("id");

            } else {
                jsonObject.put("isSelected", "0");
            }
        }
        SettingsActivity.clearMyLocation();


        notifyDataSetChanged();


    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cityName, addressName;
        ImageView markerLocation, isSelected;

        public MyViewHolder(View itemView) {
            super(itemView);
            addressName = (TextView) itemView.findViewById(R.id.addressName);
            cityName = (TextView) itemView.findViewById(R.id.cityName);
            markerLocation = (ImageView) itemView.findViewById(R.id.markerLocation);
            isSelected = (ImageView) itemView.findViewById(R.id.isSelected);
        }


    }
}
