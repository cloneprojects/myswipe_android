package com.app.myswipe.GoogleBilling;

/*
 * Created by Poyya on 26-06-2018.
 */

import android.app.Activity;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;

import java.util.ArrayList;
import java.util.List;

public class BillingManager implements PurchasesUpdatedListener {

    private Activity activity;
    private BillingClient mBillingClient;
    private BillingUpdatesListener billingUpdatesListener;
    private String TAG = BillingManager.class.getSimpleName();
    private boolean isServiceConnected = false;
    private final List<Purchase> mPurchases = new ArrayList<>();

    public BillingManager() {
    }

    public BillingManager(Activity activity, final BillingUpdatesListener billingUpdatesListener) {
        this.activity = activity;
        this.billingUpdatesListener = billingUpdatesListener;
        mBillingClient = BillingClient.newBuilder(activity).setListener(this).build();
        startServiceConnection(new Runnable() {
            @Override
            public void run() {
                billingUpdatesListener.onBillingClientSetupFinished();
            }
        });
    }

    private void startServiceConnection(final Runnable runnable) {

        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    // The billing client is ready. You can query purchases here.
                    isServiceConnected = true;
                    if (runnable != null) {
                        runnable.run();
                    }

                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                isServiceConnected = false;
            }
        });
    }

    private void executeServiceRequest(Runnable runnable) {
        if (isServiceConnected) {
            runnable.run();
        } else {
            // If the billing service disconnects, try to reconnect once.
            startServiceConnection(runnable);
        }
    }

    //final @BillingClient.SkuType String billingType
    public void initiatePurchaseFlow(final String skuId, final ArrayList<String> oldSkus) {

        Runnable purchaseFlowRequest = new Runnable() {
            @Override
            public void run() {
                BillingFlowParams mParams = BillingFlowParams.newBuilder()
                        .setSku(skuId)
                        .setType(BillingClient.SkuType.SUBS)
                        .build();
                mBillingClient.launchBillingFlow(activity, mParams);
            }
        };
        executeServiceRequest(purchaseFlowRequest);

    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        Log.e(TAG, "onPurchasesUpdated: " + responseCode);
        if (responseCode == BillingClient.BillingResponse.OK) {

            if (purchases != null) {
                Log.e(TAG, "onPurchasesUpdated: " + "purchases:" + purchases);
                mPurchases.addAll(purchases);
            }
            billingUpdatesListener.onPurchasesUpdated(mPurchases, responseCode);

        } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
            Log.i(TAG, "onPurchasesUpdated() - user cancelled the purchase flow - skipping");
            billingUpdatesListener.onPurchasesUpdated(mPurchases, responseCode);

        } else {
            Log.w(TAG, "onPurchasesUpdated() got unknown resultCode: " + responseCode);
            billingUpdatesListener.onPurchasesUpdated(mPurchases, responseCode);
        }

    }

    // returns purchased item list
    public void queryPurchases() {
        Runnable queryToExecute = new Runnable() {
            @Override
            public void run() {
                long time = System.currentTimeMillis();
                Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
                if (areSubscriptionsSupported()) {
                    Log.e(TAG, "areSubscriptionsSupported: True:" + purchasesResult.getResponseCode());
                    Purchase.PurchasesResult subscriptionResult
                            = mBillingClient.queryPurchases(BillingClient.SkuType.SUBS);
                    Log.e(TAG, "subscriptionResult: " + subscriptionResult.getResponseCode());
                    if (subscriptionResult.getResponseCode() == BillingClient.BillingResponse.OK) {

                        purchasesResult.getPurchasesList().addAll(
                                subscriptionResult.getPurchasesList());
                    } else {
                        // Handle any error response codes.
                    }
                } else if (purchasesResult.getResponseCode() == BillingClient.BillingResponse.OK) {
                    Log.e(TAG, "areSubscriptionsSupported: false:" + purchasesResult.getResponseCode());
                    // Skip subscription purchases query as they are not supported.
                } else {
                    Log.e(TAG, "areSubscriptionsSupported:-error: " + purchasesResult.getResponseCode());
                    // Handle any other error response codes.
                }
                onQueryPurchasesFinished(purchasesResult);
            }
        };
        executeServiceRequest(queryToExecute);
    }

    private void onQueryPurchasesFinished(Purchase.PurchasesResult result) {
        // Have we been disposed of in the meantime? If so, or bad result code, then quit
        if (mBillingClient == null || result.getResponseCode() != BillingClient.BillingResponse.OK) {
            Log.w(TAG, "Billing client was null or result code (" + result.getResponseCode()
                    + ") was bad – quitting");
            return;
        }

        Log.d(TAG, "Query inventory was successful.");

        // Update the UI and purchases inventory with new list of purchases
        mPurchases.clear();

        Log.d(TAG, "onPurchasesUpdatedRC: " + result.getResponseCode() + "/" + result.getPurchasesList());
        onPurchasesUpdated(BillingClient.BillingResponse.OK, result.getPurchasesList());
    }

    private boolean areSubscriptionsSupported() {
        int responseCode = mBillingClient.isFeatureSupported(BillingClient.FeatureType.SUBSCRIPTIONS);
        if (responseCode != BillingClient.BillingResponse.OK) {
            Log.w(TAG, "areSubscriptionsSupported() got an error response: " + responseCode);
        }
        Log.d(TAG, "areSubscriptionsSupported:responseCode" + responseCode);
        return responseCode == BillingClient.BillingResponse.OK;
    }




    public interface BillingUpdatesListener {
        void onBillingClientSetupFinished();

        void onConsumeFinished(String token, @BillingClient.BillingResponse int result);

        void onPurchasesUpdated(List<Purchase> purchases, int responseCode);
    }

}